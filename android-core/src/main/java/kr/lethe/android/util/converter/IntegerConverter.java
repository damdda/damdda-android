package kr.lethe.android.util.converter;



public class IntegerConverter extends NumberConverter {
    public IntegerConverter() {
    }

    public IntegerConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Integer.class;
    }
}
