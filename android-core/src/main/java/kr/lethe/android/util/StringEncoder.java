package kr.lethe.android.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * 문자를 인코딩하는 클래스
 * @author 박상철
 */
public class StringEncoder {
	/**
	 * 문자셋(기본 : UTF-8)
	 */
	private String mCharset = "UTF-8";
	
	public StringEncoder(){
		
	}
	
	public StringEncoder(String charset){
		setCharset(charset);
	}
	
	/**
	 * 문자셋 설정
	 * @param charset
	 */
	public void setCharset(String charset) {
		this.mCharset = charset;
	}

	/**
	 * 문자셋 반환
	 * @return
	 */
	public String getCharset() {
		return mCharset;
	}

	
	/**
	 * 문자열을 인코딩한다.
	 * @param in 입력
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public ByteArrayInputStream encode(InputStream in) throws UnsupportedEncodingException{
		return encode(in, null);
	}
	
	/**
	 * 문자열 인코딩한다.
	 * @param in 입력
	 * @param inCharset 입력데이터 문자셋
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public ByteArrayInputStream encode(InputStream in, String inCharset) throws UnsupportedEncodingException{
		ByteArrayInputStream result;
		InputStreamReader isr = null;
		if(inCharset != null){
			try {
				isr = new InputStreamReader(in, inCharset);
			}catch (UnsupportedEncodingException e){
				e.printStackTrace();
				isr = new InputStreamReader(in);
			}
		}else{
			isr = new InputStreamReader(in);
		}
		
		
		StringBuffer out = new StringBuffer();
        char[] buf = new char[1024];
        int len;
        try {
			while ((len = isr.read(buf)) > 0){
				out.append(new String(buf, 0, len));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		result = new ByteArrayInputStream(out.toString().getBytes(getCharset()));
		return result;
	}
}
