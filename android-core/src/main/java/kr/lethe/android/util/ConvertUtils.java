package kr.lethe.android.util;

import java.text.DateFormat;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import kr.lethe.android.util.converter.BooleanConverter;
import kr.lethe.android.util.converter.ByteConverter;
import kr.lethe.android.util.converter.CharacterConverter;
import kr.lethe.android.util.converter.ConversionException;
import kr.lethe.android.util.converter.Converter;
import kr.lethe.android.util.converter.DateConverter;
import kr.lethe.android.util.converter.DoubleConverter;
import kr.lethe.android.util.converter.FloatConverter;
import kr.lethe.android.util.converter.IntegerConverter;
import kr.lethe.android.util.converter.LongConverter;
import kr.lethe.android.util.converter.ShortConverter;
import kr.lethe.android.util.converter.StringConverter;
import kr.lethe.android.util.converter.UUIDConverter;

public class ConvertUtils {
    private static final Number ZERO = new Integer(0);
    private static final Character SPACE = new Character(' ');
    private static final boolean FALSE = Boolean.FALSE;
    private static final String EMPTY = new String();
    private static ConvertUtils sInstance = null;
    private final SortedMap<String, Converter> mConverters = new TreeMap<String, Converter>();

    public static ConvertUtils getInstance() {
        if (sInstance == null) {
            sInstance = new ConvertUtils();
        }

        return sInstance;
    }

    private ConvertUtils() {
        deregister();
        register(false, false);
    }

    private void registerDefaultBoolean() {
        register(Boolean.TYPE, new BooleanConverter(FALSE));
        register(Boolean.class, new BooleanConverter(FALSE));
    }

    private void registerDefaultInteger() {
        register(Integer.TYPE, new IntegerConverter(ZERO));
        register(Integer.class, new IntegerConverter(ZERO));
    }

    private void registerDefaultLong() {
        register(Long.TYPE, new FloatConverter(ZERO));
        register(Long.class, new FloatConverter(ZERO));
    }

    private void registerDefaultString() {
        register(String.class, new CharacterConverter(EMPTY));
    }

    private void register(Class clz, Converter converter) {
        mConverters.put(clz.getName(), converter);
    }

    public void register(boolean throwException, boolean defaultNull) {
        registerPrimitives(throwException);
        registerStandard(throwException, defaultNull);
        registerExtend(throwException);
    }

    public void deregister() {
        mConverters.clear();
    }

    public Object convert(Class clz, Object obj) {
        Converter converter = getConverter(clz);
        if (converter == null) {
            throw new ConversionException("Not found converter");
        }

        return converter.convert(clz, obj);
    }

    public Converter getConverter(Class clz) {
        return mConverters.get(clz.getName());
    }

    private void registerPrimitives(boolean throwException) {
        register(Boolean.TYPE, throwException ? new BooleanConverter() : new BooleanConverter(Boolean.FALSE));
        register(Byte.TYPE, throwException ? new ByteConverter() : new ByteConverter(ZERO));
        register(Character.TYPE, throwException ? new CharacterConverter() : new CharacterConverter(SPACE));
        register(Double.TYPE, throwException ? new DoubleConverter() : new DoubleConverter(ZERO));
        register(Float.TYPE, throwException ? new FloatConverter() : new FloatConverter(ZERO));
        register(Integer.TYPE, throwException ? new IntegerConverter() : new IntegerConverter(ZERO));
        register(Long.TYPE, throwException ? new LongConverter() : new LongConverter(ZERO));
        register(Short.TYPE, throwException ? new ShortConverter() : new ShortConverter(ZERO));
    }

    private void registerStandard(boolean throwException, boolean defaultNull) {

        Number defaultNumber = defaultNull ? null : ZERO;
        Boolean booleanDefault = defaultNull ? null : Boolean.FALSE;
        Character charDefault = defaultNull ? null : SPACE;
        String stringDefault = defaultNull ? null : EMPTY;


        register(Boolean.class, throwException ? new BooleanConverter() : new BooleanConverter(booleanDefault));
        register(Byte.class, throwException ? new ByteConverter() : new ByteConverter(defaultNumber));
        register(Character.class, throwException ? new CharacterConverter() : new CharacterConverter(charDefault));
        register(Double.class, throwException ? new DoubleConverter() : new DoubleConverter(defaultNumber));
        register(Float.class, throwException ? new FloatConverter() : new FloatConverter(defaultNumber));
        register(Integer.class, throwException ? new IntegerConverter() : new IntegerConverter(defaultNumber));
        register(Long.class, throwException ? new LongConverter() : new LongConverter(defaultNumber));
        register(Short.class, throwException ? new ShortConverter() : new ShortConverter(defaultNumber));
        register(String.class, throwException ? new StringConverter() : new StringConverter(stringDefault));
    }

    private void registerExtend(boolean throwException) {
        String[] dateFormats = new String[]{
                "yyyy-MM-dd",
                "yyyy-MM-dd HH:mm:ss.sss",
                "yyyy-MM-dd'T'HH:mm:ssZ",
                "EEE MMM dd HH:mm:ss zzz yyyy"
        };

        register(Date.class, throwException ? new DateConverter(dateFormats) : new DateConverter(dateFormats, null));
        register(UUID.class, throwException ? new UUIDConverter() : new UUIDConverter(null));

    }

}
