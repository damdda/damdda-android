package kr.lethe.android.util.converter;


public class BooleanConverter extends AbstractConverter {
    public final static String[] DEFAULT_TRUE = new String[]{"yes", "y", "true", "on", "1"};
    public final static String[] DETAULT_FALSE = new String[]{"no", "n", "false", "off", "0"};
    private final String[] mTrueStrings;
    private final String[] mFalseStrings;

    public BooleanConverter() {
        this(DEFAULT_TRUE, DETAULT_FALSE, Boolean.FALSE);
        mUseDefault = false;
    }

    public BooleanConverter(Object defaultValue) {
        this(DEFAULT_TRUE, DETAULT_FALSE, defaultValue);
    }

    public BooleanConverter(String[] trueStrings, String[] falseStrings) {
        this(trueStrings, falseStrings, Boolean.FALSE);
        mUseDefault = false;
    }

    public BooleanConverter(String[] trueStrings, String[] falseStrings, Object defaultValue) {
        super(defaultValue);
        mTrueStrings = trueStrings;
        mFalseStrings = falseStrings;
    }


    @Override
    protected Object convertToType(Class type, Object value) {
        String strValue = value.toString();
        for (String trueString : mTrueStrings) {
            if (strValue.toLowerCase() == trueString.toLowerCase()) {
                return Boolean.TRUE;
            }
        }
        for (String falseString : mFalseStrings) {
            if (strValue.toLowerCase() == falseString.toLowerCase()) {
                return Boolean.FALSE;
            }
        }
        throw new ConversionException("Can't convert value '" + value + "' to a Boolean");
    }

    @Override
    public Class getDefaultType() {
        return Boolean.class;
    }
}
