package kr.lethe.android.util.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;

public abstract class NumberConverter extends AbstractConverter {
    private static final Number ONE = new Integer(1);
    private static final Number ZERO = new Integer(0);

    protected NumberConverter() {
    }

    protected NumberConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    protected Object convertToType(Class type, Object value) {
        if (value instanceof Number) {
            return convertToNumber(type, (Number) value);
        }

        // Handle Boolean
        if (value instanceof Boolean) {
            return convertToNumber(type, ((Boolean) value).booleanValue() ? ONE : ZERO);
        }

        String stringValue = value.toString().trim();
        if (stringValue.length() == 0) {
            if (isUseDefault()) {
                return getDefaultValue();
            } else {
                throw new ConversionException(new NumberFormatException());
            }
        }

        try {
            return convertToNumber(type, NumberFormat.getInstance().parse(stringValue));
        } catch (ParseException e) {
            throw new ConversionException(e);
        }
    }

    protected Number convertToNumber(Class type, Number value) {
        Class sourceType = value.getClass();
        // Correct Number type already
        if (type.equals(sourceType)) {
            return value;
        }

        // Byte
        if (type.equals(Byte.class)) {
            long longValue = value.longValue();
            if (longValue > Byte.MAX_VALUE) {
                throw new ConversionException(sourceType + " value '" + value
                        + "' is too large for " + type);
            }
            if (longValue < Byte.MIN_VALUE) {
                throw new ConversionException(sourceType + " value '" + value
                        + "' is too small " + type);
            }
            return new Byte(value.byteValue());
        }

        // Short
        if (type.equals(Short.class)) {
            long longValue = value.longValue();
            if (longValue > Short.MAX_VALUE) {
                throw new ConversionException(sourceType + " value '" + value + "' is too large for " + type);
            }
            if (longValue < Short.MIN_VALUE) {
                throw new ConversionException(sourceType + " value '" + value + "' is too small " + type);
            }
            return new Short(value.shortValue());
        }

        // Integer
        if (type.equals(Integer.class)) {
            long longValue = value.longValue();
            if (longValue > Integer.MAX_VALUE) {
                throw new ConversionException(sourceType + " value '" + value + "' is too large for " + type);
            }
            if (longValue < Integer.MIN_VALUE) {
                throw new ConversionException(sourceType + " value '" + value + "' is too small " + type);
            }
            return new Integer(value.intValue());
        }

        // Long
        if (type.equals(Long.class)) {
            return new Long(value.longValue());
        }

        // Float
        if (type.equals(Float.class)) {
            if (value.doubleValue() > Float.MAX_VALUE) {
                throw new ConversionException(sourceType + " value '" + value + "' is too large for " + type);
            }
            return new Float(value.floatValue());
        }

        // Double
        if (type.equals(Double.class)) {
            return new Double(value.doubleValue());
        }

        // BigDecimal
        if (type.equals(BigDecimal.class)) {
            if (value instanceof Float || value instanceof Double) {
                return new BigDecimal(value.toString());
            } else if (value instanceof BigInteger) {
                return new BigDecimal((BigInteger) value);
            } else {
                return BigDecimal.valueOf(value.longValue());
            }
        }

        // BigInteger
        if (type.equals(BigInteger.class)) {
            if (value instanceof BigDecimal) {
                return ((BigDecimal) value).toBigInteger();
            } else {
                return BigInteger.valueOf(value.longValue());
            }
        }

        String msg = this.getClass() + " cannot handle conversion to '" + type + "'";
        throw new ConversionException(msg);
    }
}
