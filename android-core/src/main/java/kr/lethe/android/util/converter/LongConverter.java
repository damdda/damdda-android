package kr.lethe.android.util.converter;



public class LongConverter extends NumberConverter {
    public LongConverter() {
    }

    public LongConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Integer.class;
    }
}
