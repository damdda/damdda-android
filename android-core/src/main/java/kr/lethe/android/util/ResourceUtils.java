package kr.lethe.android.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

public class ResourceUtils {
    private static final String DIRECTORY_RESOURCE = "resource";
    private final ClassLoader mLoader;

    public ResourceUtils() {
        mLoader = ClassLoader.getSystemClassLoader();
    }

    public ResourceUtils(ClassLoader classLoader) {
        mLoader = classLoader;
    }

    private String readStream(URL url) throws IOException {
        char[] buffer = new char[4096];
        int len;

        InputStream in = url.openStream();
        Reader reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
        StringBuffer result = new StringBuffer();

        while ((len = reader.read(buffer, 0, buffer.length)) != -1) {
            result.append(buffer, 0, len);
        }
        return result.toString();
    }

    private URL getResourceUrl(String resName) {
        URL resources;
        resources = mLoader.getResource(resName);
        return resources;
    }

    public View getLayout(Context context, String name) {
        // 안드로이드 리소스로 존재할 경우 안드로이드 리소스로 레이아웃 가져오기
        String resName = String.format("@layout/%s", name);
        android.content.res.Resources androidResources = context.getResources();
        int resId = androidResources.getIdentifier(resName, "layout", context.getPackageName());
        if (resId > 0) {
            return LayoutInflater.from(context).inflate(resId, null);
        }

        String resource = getResource(context, "layout", name);

        return null;
    }

    private String getResource(Context context, String type, String name) {
        String regName = String.format("%s/%s/%s.xml", DIRECTORY_RESOURCE, type, name);
        URL resourceUrl = getResourceUrl(regName);
        if (resourceUrl == null) return null;

        try {
            return readStream(resourceUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
