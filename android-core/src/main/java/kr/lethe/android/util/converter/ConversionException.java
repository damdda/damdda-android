package kr.lethe.android.util.converter;

public class ConversionException extends RuntimeException {
    public ConversionException() {
    }

    public ConversionException(String s) {
        super(s);
    }

    public ConversionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ConversionException(Throwable throwable) {
        super(throwable);
    }
}
