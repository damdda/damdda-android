package kr.lethe.android.util.converter;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UUIDConverter extends AbstractConverter {
    private final static Pattern UUID_PATTERN = Pattern.compile("([a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12})");

    public UUIDConverter() {
    }

    public UUIDConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    protected Object convertToType(Class type, Object value) {
        if (value instanceof String) {
            Matcher matcher = UUID_PATTERN.matcher((String) value);
            if (matcher.find()) {
                return UUID.fromString(matcher.group(1));
            }
        }

        throw new ConversionException("Can't convert value '" + value + "' to a UUID");
    }

    @Override
    public Class getDefaultType() {
        return UUID.class;
    }
}
