package kr.lethe.android.util.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateConverter extends AbstractConverter {
    protected final List<SimpleDateFormat> mDateFormats = new ArrayList<SimpleDateFormat>();

    public DateConverter(String[] dateFormats) {
        for (String dateFormat : dateFormats) {
            mDateFormats.add(new SimpleDateFormat(dateFormat));
        }
    }

    public DateConverter(String[] dateFormats, Object defaultValue) {
        super(defaultValue);
        for (String dateFormat : dateFormats) {
            mDateFormats.add(new SimpleDateFormat(dateFormat));
        }
    }

    @Override
    protected Object convertToType(Class type, Object value) {
        Date date = null;
        if (value instanceof String) {
            for (SimpleDateFormat dateFormat : mDateFormats) {
                try {
                    date = dateFormat.parse((String) value);
                } catch (ParseException e) {
                }
            }

            if (date == null) {
                throw new ConversionException("Can't convert value '" + value + "' to a Date");
            }
        }
        return date;
    }

    @Override
    public Class getDefaultType() {
        return Date.class;
    }
}
