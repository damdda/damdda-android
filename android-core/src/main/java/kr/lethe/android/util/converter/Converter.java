package kr.lethe.android.util.converter;

public interface Converter {
    public Object convert(Class type, Object value);
}
