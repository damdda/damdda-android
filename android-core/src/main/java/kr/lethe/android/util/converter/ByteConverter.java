package kr.lethe.android.util.converter;



public class ByteConverter extends NumberConverter {
    public ByteConverter() {
    }

    public ByteConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Byte.class;
    }
}
