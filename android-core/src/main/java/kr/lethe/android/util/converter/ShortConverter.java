package kr.lethe.android.util.converter;



public class ShortConverter extends NumberConverter {
    public ShortConverter() {
    }

    public ShortConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Short.class;
    }
}
