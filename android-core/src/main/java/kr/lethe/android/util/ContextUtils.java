package kr.lethe.android.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;

public class ContextUtils {
    public static PackageInfo getPackageInfo(Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException ignore) {

        }
        return packageInfo;
    }

    public static String getApplicationName(Context context, String defName) {
        String label = null;

        try {
            int labelRes = context.getApplicationInfo().labelRes;
            label = context.getString(labelRes);
        } catch (Exception ignore) {
        }

        if (label == null) {
            ApplicationInfo applicationInfo = null;
            try {
                applicationInfo = context.getApplicationInfo();
            } catch (Exception ignore) {
            }
            try {
                label = applicationInfo != null ? (String) context.getPackageManager().getApplicationLabel(applicationInfo) : "";
            } catch (Exception ignore) {
            }
        }

        if (label == null) {
            label = defName;
        }

        return label;
    }

    public static Bundle getMetaData(Context context) {
        Bundle bundle = new Bundle();
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            bundle = applicationInfo.metaData;
        } catch (PackageManager.NameNotFoundException ignore) {
        }
        return bundle;
    }

    public static Object getMetaDataByName(Context context, String name) {
        Object result = null;
        try {
            result = getMetaData(context).get(name);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static int getAppVersion(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        return packageInfo == null ? -1 : packageInfo.versionCode;
    }

    public static boolean isScreenOn(Context context) {
        boolean isScreenOn = false;
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH){
            isScreenOn = ((PowerManager)context.getSystemService(Context.POWER_SERVICE)).isInteractive();
        }else{
            isScreenOn = ((PowerManager)context.getSystemService(Context.POWER_SERVICE)).isScreenOn();
        }
        return isScreenOn;
    }
}
