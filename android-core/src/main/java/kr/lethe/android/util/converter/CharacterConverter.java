package kr.lethe.android.util.converter;


public class CharacterConverter extends AbstractConverter {

    public CharacterConverter() {
    }

    public CharacterConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    protected String convertToString(Object value) {
        String strValue = value.toString();
        return strValue.length() == 0 ? "" : strValue.substring(0, 1);
    }

    @Override
    protected Object convertToType(Class type, Object value) {
        return new Character(value.toString().charAt(0));
    }

    @Override
    public Class getDefaultType() {
        return Character.class;
    }

}
