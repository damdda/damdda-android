package kr.lethe.android.util.converter;

public abstract class AbstractConverter implements Converter {
    protected Object mDefaultValue;
    protected boolean mUseDefault;


    public AbstractConverter() {
    }

    public AbstractConverter(Object defaultValue) {
        setDefaultValue(defaultValue);
    }

    public boolean isUseDefault() {
        return mUseDefault;
    }

    public void setDefaultValue(Object defaultValue) {
        mUseDefault = false;
        if (defaultValue == null) {
            mDefaultValue = null;
        } else {
            mDefaultValue = convert(getDefaultType(), defaultValue);
        }
        mUseDefault = true;
    }

    public Object getDefaultValue() {
        return mDefaultValue;
    }

    @Override
    public Object convert(Class type, Object value) {
        Class sourceType = value == null ? getDefaultType() : value.getClass();
        Class targetType = primitive(type);

        try {
            if (targetType == String.class) {
                return convertToString(value);
            } else if (targetType == null || sourceType == targetType) {
                return value;
            } else {
                return convertToType(targetType, value);
            }
        } catch (ConversionException e) {
            if (isUseDefault()) {
                return getDefaultValue();
            } else {
                throw e;
            }
        }
    }


    protected Object convertToString(Object value) {
        return value.toString();
    }

    protected Class primitive(Class type) {
        if (type == null || !type.isPrimitive()) {
            return type;
        }

        if (type == Integer.TYPE) {
            return Integer.class;
        } else if (type == Double.TYPE) {
            return Double.class;
        } else if (type == Long.TYPE) {
            return Long.class;
        } else if (type == Boolean.TYPE) {
            return Boolean.class;
        } else if (type == Float.TYPE) {
            return Float.class;
        } else if (type == Short.TYPE) {
            return Short.class;
        } else if (type == Byte.TYPE) {
            return Byte.class;
        } else if (type == Character.TYPE) {
            return Character.class;
        } else {
            return type;
        }
    }

    protected abstract Object convertToType(Class type, Object value);

    public abstract Class getDefaultType();
}
