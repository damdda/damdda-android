package kr.lethe.android.util.converter;



public class DoubleConverter extends NumberConverter {
    public DoubleConverter() {
    }

    public DoubleConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Double.class;
    }
}
