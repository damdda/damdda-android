package kr.lethe.android.util.converter;


public class StringConverter extends AbstractConverter {

    public StringConverter() {
    }

    public StringConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    protected Object convertToType(Class type, Object value) {
        return value.toString();
    }

    @Override
    public Class getDefaultType() {
        return String.class;
    }

}
