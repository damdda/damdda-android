package kr.lethe.android.util;

import android.content.Context;
import android.graphics.BitmapFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 안드로이드에서 사용할 간단한 메소드들의 집합 클래스
 * 
 * @author 박상철
 */
public class Util {
	private final static Util mInstance = new Util();

	/**
	 * Util 인스턴스를 반환
	 * @return
	 */
	public static Util getInstance(){
		return mInstance;
	}
	
	/**
	 * <p>Util 생성자</p>
	 * <p><i>Util.getInstance() 권장</i></p>
	 */
	private Util(){
	}

	/**
	 * 문자열에 html을 삭제
	 * @param content 내용
	 * @return text
	 */
	public static String removeHtml(String content) {   
	    Pattern SCRIPTS = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>",Pattern.DOTALL);   
	    Pattern STYLE = Pattern.compile("<style[^>]*>.*</style>",Pattern.DOTALL);   
	    Pattern TAGS = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");   
	    //Pattern nTAGS = Pattern.compile("<\\w+\\s+[^<]*\\s*>");   
	    Pattern ENTITY_REFS = Pattern.compile("&[^;]+;");   
	    Pattern WHITESPACE = Pattern.compile("\\s\\s+");
	       
	    Matcher m;   
	       
	    m = SCRIPTS.matcher(content);   
	    content = m.replaceAll("");   
	    m = STYLE.matcher(content);   
	    content = m.replaceAll("");   
	    m = TAGS.matcher(content);   
	    content = m.replaceAll("");   
	    m = ENTITY_REFS.matcher(content);   
	    content = m.replaceAll("");   
	    m = WHITESPACE.matcher(content);   
	    content = m.replaceAll(" ");
	    return content;   
	}
	
	/**
	 * MD5 Hash 반환
	 * @param s 문자열
	 * @return
	 */
	public static String getMD5(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String md5 = number.toString(16);
			while (md5.length() < 32)
				md5 = "0" + md5;
			return md5;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 픽셀단위를 DIP단위로 변환
	 * 
	 * @param context
	 * @param pixel 픽셀값
	 * @return DIP값
	 */
	public static int transPixeltoDip(Context context, float pixel) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pixel * scale + 0.5f);
	}

	/**
	 * 비트맵이미지 가로크기 구하기
	 * 
	 * @param fileName
	 * @return 예외 발생시 0 리턴
	 */
	public static int getBitmapOfWidth(String fileName) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(fileName, options);
			return options.outWidth;
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 비트맵이미지 세로크기 구하기
	 * 
	 * @param fileName
	 * @return 예외 발생시 0 리턴
	 */
	public static int getBitmapOfHeight(String fileName) {

		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(fileName, options);

			return options.outHeight;
		} catch (Exception e) {
			return 0;
		}
	}
}
