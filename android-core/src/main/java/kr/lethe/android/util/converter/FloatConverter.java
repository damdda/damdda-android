package kr.lethe.android.util.converter;



public class FloatConverter extends NumberConverter {
    public FloatConverter() {
    }

    public FloatConverter(Object defaultValue) {
        super(defaultValue);
    }

    @Override
    public Class getDefaultType() {
        return Float.class;
    }
}
