package kr.lethe.android.util;

import android.content.Context;

public class ScreenUtils {
    public static float convertDpToPixel(Context context, float dp) {
        if (context == null) {
            return -1;
        }
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static float convertPixelToDp(Context context, float px) {
        if (context == null) {
            return -1;
        }
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static int convertDpToPixelInt(Context context, float dp) {
        return (int)(convertDpToPixel(context, dp) + 0.5f);
    }

    public static int convertPixelToDpInt(Context context, float px) {
        return (int)(convertPixelToDp(context, px) + 0.5f);
    }
}
