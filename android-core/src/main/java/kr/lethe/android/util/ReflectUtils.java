package kr.lethe.android.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ReflectUtils {
    private static Map<String, Field> sFields = new HashMap<String, Field>();
    private static Map<String, Method> sMethods = new HashMap<String, Method>();

    public static Field getField(Class clz, String name) {
        String key = String.format("%s#%s", clz.getName(), name);
        Field field = sFields.get(key);
        if (field == null) {
            try {
                field = clz.getDeclaredField(name);
                sFields.put(key, field);
                field.setAccessible(true);
            } catch (Exception ignore) {
            }
        }

        return field;
    }

    public static Field getField(Object object, String name) {
        return getField(object.getClass(), name);
    }

    public static Object getFieldValue(Class clz, Object object, String name) {
        Field field = getField(clz, name);

        try {
            return field.get(object);
        } catch (Exception ignore) {
        }

        return null;
    }

    public static Object getFieldValue(Object object, String name) {
        return getFieldValue(object.getClass(), object, name);
    }

    public static Method getMethod(Class clz, String name) {
        String key = String.format("%s#%s", clz.getName(), name);
        Method method = sMethods.get(key);
        if (method == null) {
            try {
                method = clz.getDeclaredMethod(name);
                sMethods.put(key, method);
                method.setAccessible(true);
            } catch (Exception ignore) {
            }
        }

        return method;
    }

    public static Method getMethod(Object object, String name) {
        return getMethod(object.getClass(), name);
    }

    public static Object invokeMethod(Class clz, Object object, String name, Object... args) {
        Method method = getMethod(clz, name);

        try {
            return method.invoke(object, args);
        } catch (Exception ignore) {
        }
        return null;
    }

    public static Object invokeMethod(Object object, String name, Object... args) {
        return invokeMethod(object.getClass(), object, name, args);
    }
}
