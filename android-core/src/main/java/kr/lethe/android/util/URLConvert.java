/**
 * 
 */
package kr.lethe.android.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author 박상철
 *
 */
public class URLConvert {
    private static Map<CharSequence, CharSequence> character = new HashMap<CharSequence, CharSequence>();
    static{
        character.put("[", "%5B");
        character.put("]", "%5D");
        character.put(" ", "%20");
        character.put("+", "%2B");
        character.put(",", "%2C");
        character.put("?", "%3F");
        character.put("=", "%3D");
        character.put("&", "%26");
        character.put("&", "&amp;");
    }

    public static String encode(String encode) {
        Iterator<Entry<CharSequence, CharSequence>> iterator = character.entrySet().iterator();
        while(iterator.hasNext()) {
        	Entry<CharSequence, CharSequence> entry =  iterator.next();
        	CharSequence target = entry.getKey();
        	CharSequence replacement = entry.getValue();
            encode = encode.replace(target, replacement);
        }
        return encode;
    }

    public static String decode(String decode) {
        Iterator<Entry<CharSequence, CharSequence>> iterator = character.entrySet().iterator();
        while(iterator.hasNext()) {
        	Entry<CharSequence, CharSequence> entry =  iterator.next();
        	CharSequence target = entry.getValue();
        	CharSequence replacement = entry.getKey();
        	decode = decode.replace(target, replacement);
        }
        return decode;
    }
}


