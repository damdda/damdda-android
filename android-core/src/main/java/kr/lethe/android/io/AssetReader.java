package kr.lethe.android.io;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;


public class AssetReader extends Reader{
	private Context mContext = null;
	private InputStream mInputStream = null;
	private String mPath = null;
	
	public AssetReader(Context context, String path) {
		mContext = context;
		mPath = path;
	}

	@Override
	public void close() throws IOException {
		if(mInputStream != null){
			mInputStream.close();
		}
	}

	@Override
	public int read(char[] buf, int offset, int count) throws IOException {
		int result = -1;
		
		if(mInputStream == null){
			mInputStream = mContext.getAssets().open(mPath);
		}
		
		byte[] buffer = new byte[buf.length];
		int len = mInputStream.read(buffer, 0, count);
		if(len != -1){
			String string = new String(buffer, 0, len);
			char[] charArray = string.toCharArray();

			for(int i = 0; i < charArray.length; i++){
				buf[i] = charArray[i];
			}
			
			result = charArray.length;
		}
		return result;
	}

}
