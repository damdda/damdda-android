package kr.lethe.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class NetworkChangeReceiver extends BroadcastReceiver implements Receiver {
    private final static String TAG = NetworkChangeReceiver.class.getSimpleName();
    private final static NetworkConnectivityIntentFilter INTENT_FILTER_NETWORK_CONNECTIVITY = new NetworkConnectivityIntentFilter();
    private final Context mContext;
    private NetworkInfo mBeforeNetworkInfo = null;
    private boolean mIsRegistered = false;
    private OnNetworkChangeListener mOnNetworkChangeListener = null;


    public NetworkChangeReceiver(Context context) {
        mContext = context;
    }

    @Override
    public void registerReceiver() {
        if (!mIsRegistered) {
            mContext.registerReceiver(this, INTENT_FILTER_NETWORK_CONNECTIVITY);
            mIsRegistered = true;
        }
    }

    @Override
    public void unregisterReceiver() {
        if (mIsRegistered) {
            mContext.unregisterReceiver(this);
            mIsRegistered = false;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        onNetworkChange(activeNetwork);
    }

    private void onNetworkChange(NetworkInfo networkInfo) {
        boolean isChanged = false;
        if (!equalsNetworkInfo(mBeforeNetworkInfo, networkInfo)) {
            isChanged = true;
        }
        if (mOnNetworkChangeListener != null && isChanged) {
            mOnNetworkChangeListener.onNetworkChange(networkInfo);
        }
        mBeforeNetworkInfo = networkInfo;
    }

    public boolean equalsNetworkInfo(NetworkInfo sourceInfo, NetworkInfo targetInfo) {
        if (sourceInfo == null && targetInfo == null) {
            return true;
        }
        try {
            if (sourceInfo.getType() == targetInfo.getType()) {
                return true;
            }
        } catch (NullPointerException e) {
        }

        return false;
    }

    public OnNetworkChangeListener getOnNetworkChangeListener() {
        return mOnNetworkChangeListener;
    }

    public void setOnNetworkChangeListener(OnNetworkChangeListener onNetworkChangeListener) {
        mOnNetworkChangeListener = onNetworkChangeListener;
    }

    private static class NetworkConnectivityIntentFilter extends IntentFilter {
        private NetworkConnectivityIntentFilter() {
            addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        }
    }
}
