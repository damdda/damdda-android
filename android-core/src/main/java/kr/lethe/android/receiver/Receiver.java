package kr.lethe.android.receiver;

public interface Receiver {
    public void registerReceiver();

    public void unregisterReceiver();
}
