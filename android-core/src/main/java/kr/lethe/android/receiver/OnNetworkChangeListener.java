package kr.lethe.android.receiver;

import android.net.NetworkInfo;

public interface OnNetworkChangeListener {
    public void onNetworkChange(NetworkInfo networkInfo);
}
