package kr.lethe.android.dao;

import java.util.Map;

public interface CRUD {
    public String create();

    public String read(Map<String, Object> conditions);

    public String update();

    public String delete();
}
