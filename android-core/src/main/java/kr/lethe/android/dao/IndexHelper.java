package kr.lethe.android.dao;


import java.lang.reflect.AnnotatedElement;
import java.util.Map;

import kr.lethe.android.dao.annotation.Index;
import kr.lethe.android.dao.annotation.Order;

class IndexHelper implements CRUD {
    private final String mTableName;
    private final String mName;
    private final String[] mColumns;
    private final Order mOrder;

    public IndexHelper(String tableName, String name, String[] columns, Order order) {
        mTableName = tableName;
        mName = name;
        mColumns = columns;
        mOrder = order;
    }

    public String getName() {
        return mName;
    }

    public String[] getColumns() {
        return mColumns;
    }

    @Override
    public String create() {
        StringBuffer query = new StringBuffer();
        query.append(String.format("CREATE INDEX IF NOT EXISTS [%s] ON [%s] (", getName(), mTableName));
        String temp = "";
        for (String column : getColumns()) {
            if (temp.length() > 0) temp += ", ";
            temp += String.format("[%s] %s", column, mOrder);
        }
        query.append(temp);
        query.append(");");

        return query.toString();
    }

    @Override
    public String read(Map<String, Object> conditions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String update() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String delete() {
        return String.format("DROP INDEX IF EXISTS [%s];", mName);
    }

    public static boolean isAvailable(AnnotatedElement annotatedElement) {
        return annotatedElement.isAnnotationPresent(Index.class);
    }
}