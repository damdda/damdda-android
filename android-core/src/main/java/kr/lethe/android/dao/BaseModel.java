package kr.lethe.android.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseModel implements Model {
    private static Map<String, List<Field>> sClassFields = new HashMap<String, List<Field>>();
    private int mBeforeHashCode = -1;
    private SqliteDao mSqliteDao;

    public void setSqliteDao(SqliteDao dao) {
        mSqliteDao = dao;
    }

    @Override
    public SqliteDao getSqliteDao() {
        return mSqliteDao;
    }

    @Override
    public boolean isUpdated() {
        return mBeforeHashCode != hashCode();
    }

    @Override
    public void changed() {
        mBeforeHashCode = hashCode();
    }

    @Override
    public boolean delete() {
        SqliteDao sqliteDao = getSqliteDao();
        return sqliteDao.delete(this) > 0;
    }

    @Override
    public void save() {
        SqliteDao sqliteDao = getSqliteDao();
        if (sqliteDao == null) throw new DaoRuntimeException("SqliteDao is not set");

        if (mBeforeHashCode == -1) {
            if(sqliteDao.insert(this) > 0){
                changed();
            }
        } else if (isUpdated()) {
            if(sqliteDao.update(this) > 0){
                changed();
            }
        }
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Field field : getClassFields(this.getClass())) {
            try {
                field.setAccessible(true);
                hashCode = 31 * hashCode + field.get(this).hashCode();
            } catch (Exception e) {
            }
        }

        return hashCode;
    }

    private static List<Field> getClassFields(Class clz) {
        String className = clz.getName();
        List<Field> classFields = sClassFields.get(className);
        if (classFields == null) {
            classFields = new ArrayList<Field>();
            for (Field field : clz.getDeclaredFields()) {
                classFields.add(field);
            }
            sClassFields.put(className, classFields);
        }
        return classFields;
    }
}
