package kr.lethe.android.dao;

import java.io.Serializable;

public interface Model extends Serializable {
    public boolean isUpdated();

    public SqliteDao getSqliteDao();

    public void changed();

    public boolean delete();

    public void save();
}
