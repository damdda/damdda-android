package kr.lethe.android.dao;

public class TooManyResultsException extends DaoRuntimeException {
    public TooManyResultsException() {
    }

    public TooManyResultsException(String detailMessage) {
        super(detailMessage);
    }

    public TooManyResultsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public TooManyResultsException(Throwable throwable) {
        super(throwable);
    }
}
