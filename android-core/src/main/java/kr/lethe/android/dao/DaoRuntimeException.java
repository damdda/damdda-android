package kr.lethe.android.dao;

public class DaoRuntimeException extends RuntimeException {
    public DaoRuntimeException() {
    }

    public DaoRuntimeException(String detailMessage) {
        super(detailMessage);
    }

    public DaoRuntimeException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DaoRuntimeException(Throwable throwable) {
        super(throwable);
    }
}
