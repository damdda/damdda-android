package kr.lethe.android.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public abstract class BaseSqliteDao<T> implements SqliteDao {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
    private static Field sContentValuesField = null;
    private TableHelper mTableHelper = null;

    @Override
    public long insert(Object instance) {
        Map<String, Object> columnValues = getColumnValues(instance);
        ContentValues values = toContentValues(columnValues);
        String id = getTableHelper().getId();
        ColumnHelper idColumnHelper = getTableHelper().getColumnHelper(id);
        if (idColumnHelper != null && idColumnHelper.isAutoIncrement()) {
            values.remove(id);
        }

        return getWritableDatabase().insert(getTableHelper().getTableName(), null, values);
    }

    @Override
    public long replace(Object instance) {
        Map<String, Object> columnValues = getColumnValues(instance);
        ContentValues values = toContentValues(columnValues);
        String id = getTableHelper().getId();
        ColumnHelper idColumnHelper = getTableHelper().getColumnHelper(id);
        if (idColumnHelper != null && idColumnHelper.isAutoIncrement()) {
            values.remove(id);
        }

        return getWritableDatabase().replace(getTableHelper().getTableName(), null, values);
    }

    @Override
    public long update(Object instance) {
        Map<String, Object> columnValues = getColumnValues(instance);
        ContentValues values = toContentValues(columnValues);
        String id = getTableHelper().getId();
        values.remove(id);

        return getWritableDatabase().update(getTableHelper().getTableName(), values, String.format("%s=?", id), new String[]{String.valueOf(columnValues.get(id))});
    }

    @Override
    public long delete(Object instance) {
        Map<String, Object> columnValues = getColumnValues(instance);
        ContentValues values = toContentValues(columnValues);
        String id = getTableHelper().getId();

        return getWritableDatabase().delete(getTableHelper().getTableName(), String.format("%s=?", id), new String[]{values.getAsString(id)});
    }

    @Override
    public long deleteAll() {
        return getWritableDatabase().delete(getTableHelper().getTableName(), null, null);
    }

    @Override
    public T get(Object instance) {
        String id = getTableHelper().getId();
        Map<String, Object> columnValues = getColumnValues(instance);
        Object object = query(String.format("%s = ?", id), new String[]{String.valueOf(columnValues.get(id))}, null, null, null, null).asObject();
        return (T) object;
    }

    @Override
    public List<Object> getAll() {
        return (List<Object>) query(null, null, null, null, null, null).asList();
    }

    @Override
    public QueryWrapper<T> query(String selection, String... selectionArgs) {
        return query(selection, selectionArgs, null, null, null, null);
    }

    @Override
    public QueryWrapper<T> query(String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return new QueryWrapper<T>(getTableHelper(), getReadableDatabase().query(getTableHelper().getTableName(), new String[]{"*"}, selection, selectionArgs, groupBy, having, orderBy, limit));
    }

    @Override
    public QueryWrapper<T> rawQuery(String sql, String... selectionArgs) {
        return new QueryWrapper<T>(getTableHelper(), getReadableDatabase().rawQuery(sql, selectionArgs));
    }

    @Override
    public abstract DatabaseHelper getDatabaseHelper();

    @Override
    public TableHelper getTableHelper() {
        if (mTableHelper == null) {
            Type type = getClass().getGenericSuperclass();
            if (!(type instanceof ParameterizedType))
                throw new DaoRuntimeException("Not found parameterized type");

            ParameterizedType superClass = (ParameterizedType) type;
            Class clz = (Class) superClass.getActualTypeArguments()[0];
            mTableHelper = getDatabaseHelper().getTableHelper(clz);
        }

        return mTableHelper;
    }

    protected SQLiteDatabase getWritableDatabase() {
        return getDatabaseHelper().getWritableDatabase();
    }

    protected SQLiteDatabase getReadableDatabase() {
        return getDatabaseHelper().getReadableDatabase();
    }

    protected Map<String, Object> getColumnValues(Object instance){
        return getTableHelper().getColumnValues(instance);
    }

    protected static ContentValues toContentValues(Map<String, Object> map) {
        ContentValues contentValues = new ContentValues();
        try {
            if (sContentValuesField == null) {
                sContentValuesField = ContentValues.class.getDeclaredField("mValues");
                sContentValuesField.setAccessible(true);
            }
            Map<String, Object> values = (Map<String, Object>) sContentValuesField.get(contentValues);
            values.putAll(map);
        } catch (Exception ignore) {
            for (String key : map.keySet()) {
                try {
                    put(contentValues, key, map.get(key));
                } catch (IllegalArgumentException e) {
                }
            }
        }

        return contentValues;
    }

    private static void put(ContentValues contentValues, String name, Object object) {
        if (object == null) {
            contentValues.putNull(name);
        } else if (object instanceof Boolean) {
            contentValues.put(name, (Boolean) object);
        } else if (object instanceof Byte) {
            contentValues.put(name, (Byte) object);
        } else if (object instanceof Double) {
            contentValues.put(name, (Double) object);
        } else if (object instanceof Float) {
            contentValues.put(name, (Float) object);
        } else if (object instanceof Integer) {
            contentValues.put(name, (Integer) object);
        } else if (object instanceof Long) {
            contentValues.put(name, (Long) object);
        } else if (object instanceof Short) {
            contentValues.put(name, (Short) object);
        } else if (object instanceof String) {
            contentValues.put(name, (String) object);
        } else if (object instanceof byte[]) {
            contentValues.put(name, (byte[]) object);
        } else if (object instanceof Date) {
            contentValues.put(name, DATE_FORMAT.format(object));
        } else if (object instanceof ContentValues) {
            contentValues.putAll((ContentValues) object);
        } else {
            throw new IllegalArgumentException("can't put " + object.getClass().getName() + " in ContentValues.");
        }
    }
}
