package kr.lethe.android.dao;


import java.lang.reflect.AnnotatedElement;

import kr.lethe.android.dao.annotation.Order;
import kr.lethe.android.dao.annotation.Unique;

class UniqueHelper extends IndexHelper {
    UniqueHelper(String tableName, String name, String[] columns, Order order) {
        super(tableName, name, columns, order);
    }

    @Override
    public String create() {
        return super.create().replace("INDEX", "UNIQUE INDEX");
    }
}