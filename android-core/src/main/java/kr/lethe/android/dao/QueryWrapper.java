package kr.lethe.android.dao;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class QueryWrapper<T> {
    private final TableHelper mTableHelper;
    private final Cursor mCursor;

    QueryWrapper(TableHelper tableHelper, Cursor cursor) {
        mTableHelper = tableHelper;
        mCursor = cursor;
    }

    public Cursor asCursor() {
        return mCursor;
    }

    public List<T> asList() {
        try {
            List<T> result = new ArrayList<T>();
            while (mCursor.moveToNext()) {
                Object instance = mTableHelper.newInstance(mCursor);
                if (instance != null) {
                    if (instance instanceof Model) {
                        ((Model) instance).changed();
                    }
                    result.add((T) instance);
                } else {
                    System.out.println("객체 생성 실패!");
                }
            }
            return result;
        } finally {
            mCursor.close();
        }
    }

    public T asObject() {
        try {
            if (mCursor.getCount() >= 1) {
                mCursor.moveToFirst();
                Object instance = mTableHelper.newInstance(mCursor);
                if (instance != null && instance instanceof Model) {
                    ((Model) instance).changed();
                }
                return (T) instance;
            }
        } finally {
            mCursor.close();
        }
        return null;
    }
}