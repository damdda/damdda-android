package kr.lethe.android.dao;

public interface ColumnHelper {
    String getName();

    SqliteType getType();

    int getSize();

    Object getDefaultValue();

    boolean isNotNull();

    boolean isPrimary();

    boolean isAutoIncrement();

    Object get(Object instance);

    void set(Object instance, Object value);

    String toColumnString(boolean isInlinePrimaryKey);
}
