package kr.lethe.android.dao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    String name();

    Type type();

    boolean notnull() default false;

    String defaultValue() default "";

    boolean primary() default false;

    boolean autoincrement() default false;
}
