package kr.lethe.android.dao;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;

import kr.lethe.android.dao.annotation.Column;
import kr.lethe.android.dao.annotation.Converter;
import kr.lethe.android.util.ConvertUtils;

class ColumnHelperImpl implements ColumnHelper {
    private final Field mField;
    private String mName;
    private SqliteType mType;
    private SqliteConverter mConverter;
    private int mSize = 0;
    private Object mDefaultValue = null;
    private boolean mNotNull = false;
    private boolean mPrimary = false;
    private boolean mAutoIncrement = false;

    public ColumnHelperImpl(Field field) {
        if (!isAvailable(field)) throw new IllegalArgumentException("Not found annotation(Column)");

        Column column = field.getAnnotation(Column.class);
        Converter converter = field.getAnnotation(Converter.class);

        mField = field;
        mName = column.name();
        mType = column.type().value();
        mSize = column.type().size() == 0 ? mType.getDefaultSize() : column.type().size();
        mDefaultValue = column.defaultValue().length() > 0 ? column.defaultValue() : null;
        mNotNull = column.notnull();
        mPrimary = column.primary();
        mAutoIncrement = column.autoincrement();

        if(converter != null){
            mConverter = converter.value();
        }
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public SqliteType getType() {
        return mType;
    }

    @Override
    public int getSize() {
        return mSize;
    }

    @Override
    public Object getDefaultValue() {
        return mDefaultValue;
    }

    @Override
    public boolean isNotNull() {
        return mNotNull;
    }

    @Override
    public boolean isPrimary() {
        return mPrimary;
    }

    @Override
    public boolean isAutoIncrement() {
        return mAutoIncrement;
    }

    @Override
    public Object get(Object instance) {
        Object result = null;
        try {
            mField.setAccessible(true);
            result = mField.get(instance);

            Object data;
            if(mConverter == null){
                data = mType.toSqliteType(result);
            }else{
                data = mConverter.toSqliteType(result);
            }

            result = data;
        } catch (Exception e) {
        } finally {
            mField.setAccessible(false);
        }
        return result;
    }

    @Override
    public void set(Object instance, Object value) {
        try {
            mField.setAccessible(true);
            Object data;
            if(mConverter == null){
                data = mType.toJavaType(value);
            }else{
                data = mConverter.toJavaType(value);
            }
            mField.set(instance, data);
        } catch (Exception e) {
        } finally {
            mField.setAccessible(false);
        }
    }

    @Override
    public String toColumnString(boolean isInlinePrimaryKey) {
        StringBuffer query = new StringBuffer();
        query.append(String.format("[%s] %s", getName(), getType().getSqliteType()));
        if (getSize() > 0) {
            query.append(String.format("(%s)", getSize()));
        }
        if (isNotNull()) {
            query.append(" NOT NULL");
        }
        if (getDefaultValue() != null) {
            query.append(String.format(" DEFAULT(%s)", getDefaultValue()));
        }
        if (isInlinePrimaryKey && isPrimary()) {
            query.append(" PRIMARY KEY");
            if (isAutoIncrement()) query.append(" AUTOINCREMENT");
        }
        return query.toString();
    }

    public static boolean isAvailable(AnnotatedElement annotatedElement) {
        return annotatedElement.isAnnotationPresent(Column.class);
    }
}