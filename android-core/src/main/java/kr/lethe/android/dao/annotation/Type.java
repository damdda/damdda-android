package kr.lethe.android.dao.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import kr.lethe.android.dao.SqliteType;

@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface Type {
    SqliteType value();

    int size() default 0;

}
