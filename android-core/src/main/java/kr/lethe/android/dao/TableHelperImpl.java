package kr.lethe.android.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kr.lethe.android.dao.annotation.Id;
import kr.lethe.android.dao.annotation.Index;
import kr.lethe.android.dao.annotation.Table;

public class TableHelperImpl implements TableHelper, CRUD {
    private final Class mTableClass;
    private final String mTableName;
    private final Map<String, ColumnHelper> mColumnHelpers = new LinkedHashMap<String, ColumnHelper>();
    private final List<IndexHelper> mIndexHelpers = new ArrayList<IndexHelper>();
    private final List<String> mPrimaryColumns = new ArrayList<String>();
    private ColumnHelper mId = null;

    public TableHelperImpl(Class clz) {
        if (!isAvailable(clz)) throw new IllegalArgumentException("Not found annotation(Table)");

        Table table = (Table) clz.getAnnotation(Table.class);
        mTableName = table.name();
        mTableClass = clz;

        initColumn(clz.getDeclaredFields());
        initIndex(table.indexs());
    }

    protected void initColumn(Field[] fields) {
        for (Field field : fields) {
            if (!ColumnHelperImpl.isAvailable(field)) continue;
            ColumnHelper columnHelper = new ColumnHelperImpl(field);
            mColumnHelpers.put(columnHelper.getName(), columnHelper);

            if (columnHelper.isPrimary()) mPrimaryColumns.add(columnHelper.getName());

            if (field.isAnnotationPresent(Id.class)) {
                if (mId != null) continue;
                mId = columnHelper;
            }
        }
        if (mId == null) {
            throw new DaoRuntimeException("@Id가 필요합니다.");
        }
    }

    protected void initIndex(Index[] indexes) {
        for (Index index : indexes) {
            if (index.isUnique()) {
                mIndexHelpers.add(new UniqueHelper(getTableName(), index.name(), index.columns(), index.order()));
            } else {
                mIndexHelpers.add(new IndexHelper(getTableName(), index.name(), index.columns(), index.order()));
            }
        }
    }

    @Override
    public String getTableName() {
        return mTableName;
    }

    @Override
    public Class getTableClass() {
        return mTableClass;
    }

    @Override
    public ColumnHelper getColumnHelper(String columnName) {
        return mColumnHelpers.get(columnName);
    }

    @Override
    public String getId() {
        return mId.getName();
    }

    @Override
    public Map<String, Object> getColumnValues(Object instance) {
        if (!mTableClass.isInstance(instance))
            throw new IllegalArgumentException("instance must be an instance of the class table.");

        Map<String, Object> result = new HashMap<String, Object>();

        for (ColumnHelper columnHelper : mColumnHelpers.values()) {
            result.put(columnHelper.getName(), columnHelper.get(instance));
        }

        return result;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        List<CRUD> cruds = new ArrayList<CRUD>();
        cruds.add(this);
        cruds.addAll(mIndexHelpers);

        for (CRUD crud : cruds) {
            db.execSQL(crud.create());
        }
    }

    @Override
    public void onDrop(SQLiteDatabase db) {
        List<CRUD> cruds = new ArrayList<CRUD>();
        cruds.add(this);
        cruds.addAll(mIndexHelpers);

        for (CRUD crud : cruds) {
            db.execSQL(crud.delete());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onDrop(db);
        onCreate(db);
    }

    @Override
    public String create() {
        StringBuffer query = new StringBuffer();

        //생성 쿼리
        query.append(String.format("CREATE TABLE IF NOT EXISTS [%s] (", getTableName()));

        //컬럼명
        String columnsQuery = "";
        for (ColumnHelper columnHelper : mColumnHelpers.values()) {
            if (columnsQuery.length() > 0) columnsQuery += ", ";
            columnsQuery += columnHelper.toColumnString(mPrimaryColumns.size() == 1);
        }
        query.append(columnsQuery);

        //주키 설정
        if (mPrimaryColumns.size() > 1) {
            String primaryQuery = "";
            for (String primary : mPrimaryColumns) {
                if (primaryQuery.length() > 0) primaryQuery += ", ";
                primaryQuery += primary;
            }
            query.append(String.format(", PRIMARY KEY (%s)", primaryQuery));
        }
        query.append(");");

        for (IndexHelper indexHelper : mIndexHelpers) {
            query.append(indexHelper.toString());
        }

        return query.toString();
    }

    @Override
    public String read(Map<String, Object> conditions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String update() {
        return null;
    }

    @Override
    public Object newInstance(Cursor cursor) {
        Object instance = null;
        try {
            Constructor constructor = mTableClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            instance = constructor.newInstance();
        } catch (Exception ignore) {
            return null;
        }

        ColumnHelper columnHelper;
        String columnName;
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            columnName = cursor.getColumnName(i);
            columnHelper = mColumnHelpers.get(columnName);
            if (columnHelper == null) continue;
            columnHelper.set(instance, cursor.getString(i));
        }

        return instance;
    }

    @Override
    public String delete() {
        return String.format("DROP TABLE IF EXISTS [%s];", getTableName());
    }

    public static boolean isAvailable(AnnotatedElement annotatedElement) {
        return annotatedElement.isAnnotationPresent(Table.class);
    }
}
