package kr.lethe.android.dao.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface Index {
    String name();

    String[] columns();

    Order order() default Order.ASC;

    boolean isUnique() default false;
}
