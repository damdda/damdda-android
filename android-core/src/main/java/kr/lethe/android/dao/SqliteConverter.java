package kr.lethe.android.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public enum SqliteConverter {
    LIST_COMMA {
        private final static String SEPARATE = ", ";

        @Override
        public Object toSqliteType(Object value) {
            if (value instanceof List) {
                String result = "";
                for (Object obj : (List) value) {
                    if (result.length() > 0) result += SEPARATE;
                    result += obj.toString();
                }
                return result;
            } else {
                return value;
            }
        }

        @Override
        public Object toJavaType(Object value) {
            if (value instanceof String) {
                List result = new ArrayList();

                if (value != null) {
                    String[] split = ((String) value).split(SEPARATE);

                    for (String data : split) {
                        result.add(data);
                    }
                }

                return result;
            } else {
                return value;
            }
        }
    },
    SET_COMMA {
        private final static String SEPARATE = ", ";

        @Override
        public Object toSqliteType(Object value) {
            if (value instanceof Set) {
                String result = "";
                for (Object obj : (Set) value) {
                    if (result.length() > 0) result += SEPARATE;
                    result += obj.toString();
                }
                return result;
            } else {
                return value;
            }
        }

        @Override
        public Object toJavaType(Object value) {
            if (value instanceof String) {
                Set result = new HashSet();

                if (value != null) {
                    String[] split = ((String) value).split(SEPARATE);

                    for (String data : split) {
                        result.add(data);
                    }
                }

                return result;
            } else {
                return value;
            }
        }
    };

    public abstract Object toSqliteType(Object value);

    public abstract Object toJavaType(Object value);
}
