package kr.lethe.android.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import kr.lethe.android.util.ConvertUtils;

public enum SqliteType {
    INTEGER("INTEGER", Integer.TYPE, 10),
    LONG("INTEGER", Long.TYPE),
    REAL("REAL", Double.TYPE),
    TEXT("TEXT", String.class),
    CHAR("CHAR", String.class, 254),
    VARCHAR("VARCHAR", String.class, 254),
    BOOLEAN("NUMERIC", Boolean.class) {
        @Override
        public Object toSqliteType(Object value) {
            try {
                Boolean boolValue = (Boolean) value;
                return boolValue ? 1 : 0;
            } catch (ClassCastException e) {
                return 0;
            }
        }
    },
    DATETIME("DATETIME", Date.class) {
        @Override
        public Object toSqliteType(Object value) {
            return DATE_FORMAT.format(value);
        }
    },
    DATE("DATE", Date.class) {
        @Override
        public Object toSqliteType(Object value) {
            return DATE_FORMAT.format(value);
        }
    },
    UUID("CHAR", java.util.UUID.class, 36) {
        @Override
        public Object toSqliteType(Object value) {
            return value == null ? null : value.toString();
        }
    },
    BLOB("BLOB", byte[].class);

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

    private final String mSqliteType;
    private final int mDefaultSize;
    private final Class mJavaType;

    SqliteType(String sqliteType, Class javaType) {
        this(sqliteType, javaType, 0);
    }

    SqliteType(String sqliteType, Class clz, int size) {
        mSqliteType = sqliteType;
        mJavaType = clz;
        mDefaultSize = size;
    }

    public String getSqliteType() {
        return mSqliteType;
    }

    public Class getJavaType() {
        return mJavaType;
    }

    public Object toSqliteType(Object value) {
        return value;
    }

    public Object toJavaType(Object value) {
        return ConvertUtils.getInstance().convert(mJavaType, value);
    }

    public int getDefaultSize() {
        return mDefaultSize;
    }
}
