package kr.lethe.android.dao;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelperBuilder {
    private final String mName;
    private final int mVersion;
    private List<TableHelper> mTableHelpers = new ArrayList<TableHelper>();

    public DatabaseHelperBuilder(String name, int version) {
        mName = name;
        mVersion = version;
    }

    public DatabaseHelperBuilder withTable(Class clz) {
        mTableHelpers.add(new TableHelperImpl(clz));
        return this;
    }

    public DatabaseHelperBuilder withTable(TableHelper tableHelper) {
        mTableHelpers.add(tableHelper);
        return this;
    }

    public DatabaseHelper build(Context context) {
        return new DatabaseHelper(context, new DatabaseImpl(mName, mVersion, mTableHelpers.toArray(new TableHelper[0])));
    }
}
