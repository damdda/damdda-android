package kr.lethe.android.dao.annotation;

public enum Order {
    ASC,
    DESC;
}