package kr.lethe.android.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

public interface TableHelper {
    public String getTableName();

    public Class getTableClass();

    public ColumnHelper getColumnHelper(String columnName);

    public String getId();

    public Map<String, Object> getColumnValues(Object instance);

    public void onCreate(SQLiteDatabase db);

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

    public void onDrop(SQLiteDatabase db);

    public Object newInstance(Cursor cursor);
}
