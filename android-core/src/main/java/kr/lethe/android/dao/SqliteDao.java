package kr.lethe.android.dao;

import java.util.List;

public interface SqliteDao {
    long insert(Object instance);

    long replace(Object instance);

    long update(Object instance);

    long delete(Object instance);

    long deleteAll();

    Object get(Object instance);

    List<Object> getAll();

    QueryWrapper query(String selection, String... selectionArgs);

    QueryWrapper query(String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit);

    QueryWrapper rawQuery(String sql, String... selectionArgs);

    TableHelper getTableHelper();

    DatabaseHelper getDatabaseHelper();
}
