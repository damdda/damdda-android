package kr.lethe.android.dao;

public interface Database {
    public String getName();

    public int getVersion();

    public TableHelper getTableHelper(String tableName);

    public TableHelper getTableHelper(Class clz);

    public TableHelper[] getTableHelpers();
}
