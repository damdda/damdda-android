package kr.lethe.android.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static enum UpgradeStrategy {
        DROP_CREATE,
        UPGRADE;
    }

    private final Context mContext;
    private final Database mDatabase;

    public DatabaseHelper(Context context, Database database) {
        super(context, database.getName(), null, database.getVersion());

        mContext = context;
        mDatabase = database;
    }

    public Context getContext() {
        return mContext;
    }

    public TableHelper[] getTableHelpers() {
        return mDatabase.getTableHelpers();
    }

    public TableHelper getTableHelper(String tableName) {
        return mDatabase.getTableHelper(tableName);
    }

    public TableHelper getTableHelper(Class clz) {
        return mDatabase.getTableHelper(clz);
    }

    public UpgradeStrategy getUpgradeStrategy() {
        return UpgradeStrategy.UPGRADE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (TableHelper tableHelper : getTableHelpers()) {
            tableHelper.onCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (getUpgradeStrategy()) {
            case DROP_CREATE:
                for (TableHelper tableHelper : getTableHelpers()) {
                    tableHelper.onDrop(db);
                    tableHelper.onCreate(db);
                }
                break;
            case UPGRADE:
                for (TableHelper tableHelper : getTableHelpers()) {
                    tableHelper.onUpgrade(db, oldVersion, newVersion);
                }
                break;
        }
    }
}
