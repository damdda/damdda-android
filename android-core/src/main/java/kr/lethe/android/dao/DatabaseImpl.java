package kr.lethe.android.dao;

import java.util.HashMap;
import java.util.Map;

public class DatabaseImpl implements Database {
    private String mName;
    private int mVersion;
    private Map<String, TableHelper> mTableHelpers = new HashMap<String, TableHelper>();

    public DatabaseImpl(String name, int version, TableHelper[] tableHelpers) {
        mName = name;
        mVersion = version;
        for (TableHelper tableHelper : tableHelpers) {
            mTableHelpers.put(tableHelper.getTableName(), tableHelper);
        }
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public int getVersion() {
        return mVersion;
    }

    @Override
    public TableHelper getTableHelper(String tableName) {
        return mTableHelpers.get(tableName);
    }

    @Override
    public TableHelper getTableHelper(Class clz) {
        for (TableHelper tableHelper : mTableHelpers.values()) {
            if (tableHelper.getTableClass() == clz) {
                return tableHelper;
            }
        }
        return null;
    }

    @Override
    public TableHelper[] getTableHelpers() {
        return mTableHelpers.values().toArray(new TableHelper[0]);
    }
}
