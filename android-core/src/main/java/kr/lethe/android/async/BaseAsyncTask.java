package kr.lethe.android.async;

import android.os.AsyncTask;

public abstract class BaseAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
	protected OnAsyncTaskListener mOnAsyncTaskListener = null;
	protected boolean mIsError = false;
	private Object mExtraVar = null;


	public void setOnAsyncTaskListener(OnAsyncTaskListener onAsyncTaskListener) {
		mOnAsyncTaskListener = onAsyncTaskListener;
	}
	
	public Object getExtraVar() {
		return mExtraVar;
	}

	public void setExtraVar(Object extraVar) {
		this.mExtraVar = extraVar;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mIsError = false;
	}
	
	@Override
	protected void onProgressUpdate(Progress... values) {
		super.onProgressUpdate(values);
		if (mOnAsyncTaskListener != null) {
			mOnAsyncTaskListener.onProgress(values[0]);
		}
	}

    protected abstract Result doInBackgroundTask(Params... params);

    @Override
    protected Result doInBackground(Params... params) {
        try{
            return doInBackgroundTask(params);
        }catch(Exception e){
            onError(e);
        }
        return null;
    }

    @Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);

		if (mIsError == false && mOnAsyncTaskListener != null) {
			mOnAsyncTaskListener.onComplete(this, result);
		}
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		if (mOnAsyncTaskListener != null) {
			mOnAsyncTaskListener.onCancel();
		}
	}

	protected void onError(Throwable error) {
		mIsError = true;
		if (mOnAsyncTaskListener != null) {
			mOnAsyncTaskListener.onError(error);
		}
	}
	
	public boolean isError(){
		return mIsError;
	}
}
