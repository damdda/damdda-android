package kr.lethe.android.async;

import android.os.AsyncTask;

/**
 * Created by bad79s on 15. 2. 16..
 */
public class OnAsyncTaskListenerImpl<Params, Progress, Result> implements OnAsyncTaskListener<Params, Progress, Result> {
    @Override
    public void onProgress(Progress... progress) {

    }

    @Override
    public void onError(Throwable error) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onComplete(AsyncTask<Params, Progress, Result> sender, Result result) {

    }
}
