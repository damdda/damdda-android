package kr.lethe.android.async;

import android.os.AsyncTask;

public interface OnAsyncTaskListener<Params, Progress, Result> {
	public void onProgress(Progress... progress);
	public void onError(Throwable error);
	public void onCancel();
	public void onComplete(AsyncTask<Params, Progress, Result> sender, Result result);
}
