package kr.lethe.android.async;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class FileWriteAsyncTask extends BaseAsyncTask<InputStream, Integer, File> {
	private final static String TAG = FileWriteAsyncTask.class.getSimpleName();
	private final static int BUFFER_SIZE = 1024;

	private String mDirectory = null;
	private Context mContext = null;
	private String mFileName = null;
	
	public FileWriteAsyncTask(Context context, String directory, String fileName) {
		mContext = context;
		mDirectory = directory;
		mFileName = fileName;
	}

	public void setDirectory(String directory) {
		mDirectory = directory;
	}

	public String getDirectory() {
		return mDirectory;
	}

	public String getFileName() {
		return mFileName;
	}

	public void setFileName(String mFileName) {
		this.mFileName = mFileName;
	}

	@Override
	protected File doInBackgroundTask(InputStream... params) {
		InputStream inputStream = params[0];

		File cacheDir = new File(mDirectory);
		if (!cacheDir.exists())
			cacheDir.mkdirs();

		OutputStream outputStream = null;
		File file = null;
		try {
			file = new File(cacheDir, mFileName);

			outputStream = new FileOutputStream(file);
			byte[] buffer = new byte[BUFFER_SIZE];
			int len = -1;

			while ((len = inputStream.read(buffer, 0, buffer.length)) != -1) {
				outputStream.write(buffer, 0, len);
			}

		} catch (IOException e) {
			onError(new Error(e));
		} finally {
			try {
				outputStream.flush();
			} catch (IOException ignore) {
			}
			try {
				outputStream.close();
			} catch (IOException ignore) {
			}
			try {
				inputStream.close();
			} catch (IOException ignore) {
			}
		}

		return file;
	}
}