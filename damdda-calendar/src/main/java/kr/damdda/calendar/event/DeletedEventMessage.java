package kr.damdda.calendar.event;

import kr.damdda.calendar.domain.Event;

public final class DeletedEventMessage implements EventMessage {
    public final static String EVENT_NAME = "deleted_event";
    private final Event mEvent;

    public DeletedEventMessage(Event event) {
        mEvent = event;
    }

    public Event getEvent() {
        return mEvent;
    }
}