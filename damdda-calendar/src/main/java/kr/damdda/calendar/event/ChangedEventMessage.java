package kr.damdda.calendar.event;

import kr.damdda.calendar.domain.Event;

public final class ChangedEventMessage implements EventMessage {
    public final static String EVENT_NAME = "changed_event";
    private final Event mEvent;

    public ChangedEventMessage(Event event) {
        mEvent = event;
    }

    public Event getEvent() {
        return mEvent;
    }
}