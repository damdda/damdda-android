package kr.damdda.calendar.event;

import kr.damdda.calendar.domain.User;

public final class LoggedMessage implements EventMessage {
    public final static String EVENT_NAME = "logged";
    private final User mUser;

    public LoggedMessage(User user) {
        mUser = user;
    }

    public User getUser() {
        return mUser;
    }
}