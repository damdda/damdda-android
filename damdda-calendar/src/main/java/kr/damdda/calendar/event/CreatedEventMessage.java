package kr.damdda.calendar.event;

import kr.damdda.calendar.domain.Event;

public final class CreatedEventMessage implements EventMessage {
    public final static String EVENT_NAME = "created_event";
    private final Event mEvent;

    public CreatedEventMessage(Event event) {
        mEvent = event;
    }

    public Event getEvent() {
        return mEvent;
    }
}