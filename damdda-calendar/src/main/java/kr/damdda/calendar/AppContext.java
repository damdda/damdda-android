package kr.damdda.calendar;

import android.content.Context;
import android.content.SharedPreferences;

import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.User;
import kr.damdda.calendar.protocol.Protocol;

import java.util.Date;

import kr.lethe.android.dao.DatabaseHelper;

public interface AppContext {
    Context getContext();

    String getApiHost();

    String getApiUrl();

    String getClientId();

    String getSenderId();

    Protocol getProtocol();

    String getDatabaseName();

    int getDatabaseVersion();

    DatabaseHelper getDatabaseHelper();

    boolean isFirstStart();

    String getUdid();

    void saveUdid(String deviceUuid);

    String getRegistrationId();

    void saveRegistrationId(String registrationId);

    AccessToken getAccessToken();

    void setAccessToken(AccessToken accessToken);

    User getUser();

    void setUser(User user);

    Device getDevice();

    void setDevice(Device device);

    Date getSyncTime();

    void setSyncTime(Date syncTime);

    SharedPreferences getSharedPreferences();

    SharedPreferences.Editor getEditor();
}
