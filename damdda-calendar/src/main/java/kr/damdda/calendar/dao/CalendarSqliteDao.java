package kr.damdda.calendar.dao;

import kr.damdda.calendar.domain.Event;

import java.text.SimpleDateFormat;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.BaseSqliteDao;
import kr.lethe.android.dao.DatabaseHelper;

@DebugLog
public class CalendarSqliteDao extends BaseSqliteDao<Event> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

    public CalendarSqliteDao(DatabaseHelper databaseHelper) {
        mDatabaseHelper = databaseHelper;
    }

    private DatabaseHelper mDatabaseHelper;

    @Override
    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }
}
