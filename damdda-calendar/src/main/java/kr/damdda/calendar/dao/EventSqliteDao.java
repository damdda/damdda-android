package kr.damdda.calendar.dao;

import kr.damdda.calendar.domain.Event;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.BaseSqliteDao;
import kr.lethe.android.dao.DatabaseHelper;

@DebugLog
public class EventSqliteDao extends BaseSqliteDao<Event> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

    public EventSqliteDao(DatabaseHelper databaseHelper) {
        mDatabaseHelper = databaseHelper;
    }

    private DatabaseHelper mDatabaseHelper;

    public Event findById(UUID uuid){
        return this.query("id = ?", uuid.toString()).asObject();
    }

    public List<Event> getEventsByYearAndMonth(int year, int month){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.set(year, month - 1, 1, 0, 0, 0);
        startCalendar.set(Calendar.MILLISECOND, 0);
        Date startDate = startCalendar.getTime();

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.set(year, month, 0, 23, 59, 59);
        endCalendar.set(Calendar.MILLISECOND, 0);
        Date endDate = endCalendar.getTime();

        return this.query("start_time >= ? and end_time <= ?", dateFormat.format(startDate), dateFormat.format(endDate)).asList();
    }

    @Override
    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }
}
