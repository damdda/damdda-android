package kr.damdda.calendar.dao;

import kr.damdda.calendar.domain.Alarm;

import java.text.SimpleDateFormat;
import java.util.List;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.BaseSqliteDao;
import kr.lethe.android.dao.DatabaseHelper;

@DebugLog
public class AlarmSqliteDao extends BaseSqliteDao<Alarm> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

    public AlarmSqliteDao(DatabaseHelper databaseHelper) {
        mDatabaseHelper = databaseHelper;
    }

    private DatabaseHelper mDatabaseHelper;

    @Override
    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }
}
