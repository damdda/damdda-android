package kr.damdda.calendar.exception;

import android.os.RemoteException;

import java.io.PrintStream;
import java.io.PrintWriter;

public class DamddaRemoteException extends RemoteException {
    private final Throwable mThrowable;

    public DamddaRemoteException() {
        mThrowable = new Throwable();
    }

    public DamddaRemoteException(String detailMessage) {
        mThrowable = new Throwable(detailMessage);
    }

    public DamddaRemoteException(Throwable cause) {
        mThrowable = new Throwable(cause);
    }

    public DamddaRemoteException(String detailMessage, Throwable cause) {
        mThrowable = new Throwable(detailMessage, cause);

    }

    @Override
    public Throwable fillInStackTrace() {
        return mThrowable.fillInStackTrace();
    }

    @Override
    public String getMessage() {
        return mThrowable.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return mThrowable.getLocalizedMessage();
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return mThrowable.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] trace) {
        mThrowable.setStackTrace(trace);
    }

    @Override
    public void printStackTrace() {
        mThrowable.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream err) {
        mThrowable.printStackTrace(err);
    }

    @Override
    public void printStackTrace(PrintWriter err) {
        mThrowable.printStackTrace(err);
    }

    @Override
    public String toString() {
        return mThrowable.toString();
    }

    @Override
    public Throwable initCause(Throwable throwable) {
        return mThrowable.initCause(throwable);
    }

    @Override
    public Throwable getCause() {
        return mThrowable.getCause();
    }
}
