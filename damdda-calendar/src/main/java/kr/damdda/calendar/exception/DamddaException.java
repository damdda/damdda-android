package kr.damdda.calendar.exception;

public class DamddaException extends Exception {
    public DamddaException() {
    }

    public DamddaException(String detailMessage) {
        super(detailMessage);
    }

    public DamddaException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DamddaException(Throwable throwable) {
        super(throwable);
    }
}
