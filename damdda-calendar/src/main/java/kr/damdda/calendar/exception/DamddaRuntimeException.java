package kr.damdda.calendar.exception;

public class DamddaRuntimeException extends RuntimeException {
    public DamddaRuntimeException() {
    }

    public DamddaRuntimeException(String detailMessage) {
        super(detailMessage);
    }

    public DamddaRuntimeException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DamddaRuntimeException(Throwable throwable) {
        super(throwable);
    }
}
