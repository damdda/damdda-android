package kr.damdda.calendar.cordova;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import org.apache.cordova.CordovaPlugin;

import hugo.weaving.DebugLog;


@DebugLog
public class IntentPlugin  extends CordovaPlugin{
    public static final String INTENT_PROTOCOL_START = "intent:";
    public static final String INTENT_PROTOCOL_INTENT = "#Intent;";
    public static final String INTENT_PROTOCOL_END = ";end;";
    public static final String GOOGLE_PLAY_STORE_PREFIX = "market://details?id=";

    @Override
    public boolean onOverrideUrlLoading(String url) {

        if (url.startsWith(INTENT_PROTOCOL_START)) {
            final int customUrlStartIndex = INTENT_PROTOCOL_START.length();
            final int customUrlEndIndex = url.indexOf(INTENT_PROTOCOL_INTENT);
            if (customUrlEndIndex < 0) {
                return false;
            } else {
                final String customUrl = url.substring(customUrlStartIndex, customUrlEndIndex);
                try {
                    this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(customUrl)));
                } catch (ActivityNotFoundException e) {
                    final int packageStartIndex = customUrlEndIndex + INTENT_PROTOCOL_INTENT.length();
                    final int packageEndIndex = url.indexOf(INTENT_PROTOCOL_END);

                    final String packageName = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex);
                    this.webView.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + packageName)));
                }
                return true;
            }
        } else {
            return false;
        }
    }
}
