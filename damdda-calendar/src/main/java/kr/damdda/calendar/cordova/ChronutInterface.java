package kr.damdda.calendar.cordova;

import com.google.gson.JsonSyntaxException;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.User;
import kr.damdda.calendar.event.ChangedEventMessage;
import kr.damdda.calendar.event.CreatedEventMessage;
import kr.damdda.calendar.event.DeletedEventMessage;
import kr.damdda.calendar.event.LoggedMessage;
import kr.damdda.calendar.event.LogoutMessage;
import kr.damdda.calendar.event.RejectMessage;
import kr.damdda.calendar.json.AppGsonBuilder;

@DebugLog
public class ChronutInterface extends CordovaPlugin {
    private AppContext mAppContext;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        mAppContext = AppContextFactory.getInstance(cordova.getActivity());
    }

    public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        switch (action) {
            case "setAccessToken":
                final JSONObject jsonObject = args.optJSONObject(0);
                if (jsonObject == null) {
                    callbackContext.error("Invalidate AccessToken");
                } else {
                    try {
                        this.cordova.getThreadPool().execute(new Runnable() {
                            @Override
                            public void run() {
                                AccessToken accessToken = AppGsonBuilder.DEFAULT.fromJson(jsonObject.toString(), AccessToken.class);
                                mAppContext.setAccessToken(accessToken);
                            }
                        });
                        callbackContext.success();
                    } catch (JsonSyntaxException e) {
                        callbackContext.error("Invalidate AccessToken");
                    }
                }
                break;
            case "getAccessToken":
                AccessToken accessToken = mAppContext.getAccessToken();
                if (accessToken == null) {
                    callbackContext.success();
                } else {
                    JSONObject accessTokenObject = new JSONObject(AppGsonBuilder.DEFAULT.toJson(accessToken));
                    callbackContext.success(accessTokenObject);
                }
                break;
            case "getUDID":
                callbackContext.success(mAppContext.getUdid());
                break;
            case "fire":
                parseFire(args, callbackContext);
                break;

        }
        return false;
    }

    private boolean parseFire(CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        String eventName = args.getString(0);
        final JSONObject data = args.optJSONObject(1);
        if (eventName == null) {
            callbackContext.error("Uncaught Function required as first argument!");
            return false;
        }

        switch (eventName) {
            case LoggedMessage.EVENT_NAME:
                if (data == null) {
                    callbackContext.error("require data");
                    return false;
                }

                try {
                    this.cordova.getThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            User user = AppGsonBuilder.DEFAULT.fromJson(data.toString(), User.class);
                            EventBus.getDefault().post(new LoggedMessage(user));
                        }
                    });
                    callbackContext.success();
                } catch (JsonSyntaxException e) {
                    callbackContext.error(e.getMessage());
                }

                break;
            case LogoutMessage.EVENT_NAME:
                this.cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new LogoutMessage());
                    }
                });

                callbackContext.success();
                break;
            case RejectMessage.EVENT_NAME:
                this.cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new LogoutMessage());
                    }
                });

                callbackContext.success();
                break;

            case CreatedEventMessage.EVENT_NAME:
                try {
                    this.cordova.getThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            Event event = AppGsonBuilder.DEFAULT.fromJson(data.toString(), Event.class);
                            EventBus.getDefault().post(new CreatedEventMessage(event));
                        }
                    });
                    callbackContext.success();
                } catch (JsonSyntaxException e) {
                    callbackContext.error(e.getMessage());
                }
                break;
            case ChangedEventMessage.EVENT_NAME:
                try {
                    this.cordova.getThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            Event event = AppGsonBuilder.DEFAULT.fromJson(data.toString(), Event.class);
                            EventBus.getDefault().post(new ChangedEventMessage(event));
                        }
                    });
                    callbackContext.success();
                } catch (JsonSyntaxException e) {
                    callbackContext.error(e.getMessage());
                }
                break;
            case DeletedEventMessage.EVENT_NAME:
                try {
                    this.cordova.getThreadPool().execute(new Runnable() {
                        @Override
                        public void run() {
                            Event event = AppGsonBuilder.DEFAULT.fromJson(data.toString(), Event.class);
                            EventBus.getDefault().post(new DeletedEventMessage(event));
                        }
                    });
                    callbackContext.success();
                } catch (JsonSyntaxException e) {
                    callbackContext.error(e.getMessage());
                }
                break;
            default:
                callbackContext.error("Not found event name.");
                break;
        }

        return false;
    }
}
