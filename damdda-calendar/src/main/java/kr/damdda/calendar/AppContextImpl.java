package kr.damdda.calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.User;
import kr.damdda.calendar.exception.DamddaRuntimeException;
import kr.damdda.calendar.json.AppGsonBuilder;
import kr.damdda.calendar.protocol.Protocol;
import kr.damdda.calendar.protocol.ProtocolFactory;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.UUID;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.DatabaseHelper;
import kr.lethe.android.dao.DatabaseHelperBuilder;
import kr.lethe.android.util.ContextUtils;

@DebugLog
public class AppContextImpl implements AppContext {
    public static final String META_HOST = "kr.damdda.calendar.meta.host";
    public static final String META_CLIENT_ID = "kr.damdda.calendar.meta.clientId";
    public static final String META_SENDER_ID = "kr.damdda.calendar.meta.senderId";
    public static final String META_DATABASE_VERSION = "kr.damdda.calendar.meta.database.version";
    public static final String META_DATABASE_NAME = "kr.damdda.calendar.meta.database.name";
    public static final String PREFERENCES = "damdda";
    public static final String PREFERENCE_DEVICE_ID = "udid";
    public static final String PREFERENCE_REGISTRATION_ID = "registration_id";
    public static final String PREFERENCE_ACCESS_TOKEN = "access_token";
    public static final String PREFERENCE_REGISTRATION_USER = "registration_user";
    public static final String PREFERENCE_REGISTRATION_DEVICE = "registration_device";
    public static final String PREFERENCE_EVENT_SYNCDATE = "event_syncdate";

    private Context mContext;
    private String mApiHost;
    private String mClientId;
    private String mSenderId;
    private Protocol mProtocol;
    private String mDatabaseName;
    private int mDatabaseVersion;
    private AccessToken mAccessToken = null;

    public AppContextImpl(Context context) {
        mContext = context;

        Context tmpContext = context.getApplicationContext() == null ? context : context.getApplicationContext();
        Bundle metaData = ContextUtils.getMetaData(tmpContext);

        mApiHost = metaData.getString(META_HOST);

        if (mApiHost == null) {
            throw new DamddaRuntimeException(String.format("메타데이터에서 API HOST(%s)를 찾을 수 없습니다.", META_HOST));
        }

        mClientId = metaData.getString(META_CLIENT_ID);

        if (mClientId == null) {
            throw new DamddaRuntimeException(String.format("메타데이터에서 클라이언트 고유키(%s)를 찾을 수 없습니다.", META_CLIENT_ID));
        }

        mSenderId = metaData.getString(META_SENDER_ID);

        if (mSenderId == null) {
            throw new DamddaRuntimeException(String.format("메타데이터에서 GCM Sender ID(%s)를 찾을 수 없습니다.", META_SENDER_ID));
        }

        mDatabaseName = metaData.getString(META_DATABASE_NAME);

        if (mDatabaseName == null) {
            throw new DamddaRuntimeException(String.format("메타데이터에서 데이터베이스 이름(%s)를 찾을 수 없습니다.", META_DATABASE_NAME));
        }

        mDatabaseVersion = metaData.getInt(META_DATABASE_VERSION, -1);

        if (mDatabaseVersion == -1) {
            throw new DamddaRuntimeException(String.format("메타데이터에서 데이터베이스 버전(%s)를 찾을 수 없습니다.", META_DATABASE_VERSION));
        }
    }


    @Override
    public Context getContext() {
        return mContext;
    }

    @Override
    public String getApiHost() {
        return mApiHost;
    }


    @Override
    public String getApiUrl() {
        return getApiHost();
    }

    @Override
    public String getClientId() {
        return mClientId;
    }

    @Override
    public String getSenderId() {
        return mSenderId;
    }

    @Override
    public Protocol getProtocol() {
        if (mProtocol == null) {
            mProtocol = AppProtocol.getInstance(ProtocolFactory.create(getApiUrl()));
        }

        AccessToken accessToken = getAccessToken();
        mProtocol.addHeader(Protocol.HEADER_AUTHORIZATION, accessToken == null ? null : accessToken.toString());

        return mProtocol;
    }

    @Override
    public String getDatabaseName() {
        return mDatabaseName;
    }

    @Override
    public int getDatabaseVersion() {
        return mDatabaseVersion;
    }

    @Override
    public DatabaseHelper getDatabaseHelper() {
        return new DatabaseHelperBuilder(getDatabaseName(), getDatabaseVersion()).withTable(Calendar.class).withTable(Event.class).withTable(Alarm.class).build(getContext());
    }

    @Override
    public boolean isFirstStart() {
        return getUdid() == null ? true : false;
    }

    @Override
    public String getUdid() {
        String deviceUuid = getSharedPreferences().getString(PREFERENCE_DEVICE_ID, null);
        if (deviceUuid == null) {
            deviceUuid = createUdid(getContext());
            saveUdid(deviceUuid);
        }
        return deviceUuid;
    }

    @Override
    public void saveUdid(String deviceUuid) {
        if (deviceUuid != null) {
            getEditor().putString(PREFERENCE_DEVICE_ID, deviceUuid).commit();
        } else {
            getEditor().remove(PREFERENCE_DEVICE_ID).commit();
        }
    }

    @Override
    public String getRegistrationId() {
        return getSharedPreferences().getString(PREFERENCE_REGISTRATION_ID, null);
    }

    @Override
    public void saveRegistrationId(String registrationId) {
        if (registrationId != null) {
            getEditor().putString(PREFERENCE_REGISTRATION_ID, registrationId).commit();
        } else {
            getEditor().remove(PREFERENCE_REGISTRATION_ID).commit();
        }
    }

    @Override
    public AccessToken getAccessToken() {
        if (mAccessToken == null) {
            AccessToken accessToken = null;
            String accessTokenJson = getSharedPreferences().getString(PREFERENCE_ACCESS_TOKEN, null);
            if (accessTokenJson != null) {
                try {
                    accessToken = AppGsonBuilder.DEFAULT_FORMAT.fromJson(accessTokenJson, AccessToken.class);
                } catch (JsonSyntaxException ignore) {
                    getEditor().remove(PREFERENCE_ACCESS_TOKEN).commit();
                }
            }
            mAccessToken = accessToken;
        }
        return mAccessToken;
    }

    @Override
    public void setAccessToken(AccessToken accessToken) {
        mAccessToken = accessToken;
        String accessTokenJson = AppGsonBuilder.DEFAULT_FORMAT.toJson(accessToken);
        if (accessTokenJson != null) {
            getEditor().putString(PREFERENCE_ACCESS_TOKEN, accessTokenJson).commit();
        } else {
            getEditor().remove(PREFERENCE_ACCESS_TOKEN);
        }
    }

    @Override
    public User getUser() {
        User user = null;
        String userJson = getSharedPreferences().getString(PREFERENCE_REGISTRATION_USER, null);
        if (userJson != null) {
            try {
                user = AppGsonBuilder.DEFAULT_FORMAT.fromJson(userJson, User.class);
            } catch (JsonSyntaxException ignore) {
                getEditor().remove(PREFERENCE_REGISTRATION_USER).commit();
            }
        }

        return user;
    }

    @Override
    public void setUser(User user) {
        String userJson = AppGsonBuilder.DEFAULT_FORMAT.toJson(user);
        if (userJson != null) {
            getEditor().putString(PREFERENCE_REGISTRATION_USER, userJson).commit();
        } else {
            getEditor().remove(PREFERENCE_REGISTRATION_USER);
        }
    }

    @Override
    public Device getDevice() {
        Device device = null;
        String deviceJson = getSharedPreferences().getString(PREFERENCE_REGISTRATION_DEVICE, null);
        if (deviceJson != null) {
            try {
                device = AppGsonBuilder.DEFAULT_FORMAT.fromJson(deviceJson, Device.class);
            } catch (JsonSyntaxException ignore) {
                getEditor().remove(PREFERENCE_REGISTRATION_DEVICE).commit();
            }
        }

        return device;
    }

    @Override
    public void setDevice(Device device) {
        String deviceJson = AppGsonBuilder.DEFAULT_FORMAT.toJson(device);
        if (deviceJson != null) {
            getEditor().putString(PREFERENCE_REGISTRATION_DEVICE, deviceJson).commit();
        } else {
            getEditor().remove(PREFERENCE_REGISTRATION_DEVICE);
        }
    }

    @Override
    public Date getSyncTime() {
        Date syncTime = null;
        String syncTimeJson = getSharedPreferences().getString(PREFERENCE_EVENT_SYNCDATE, null);
        if (syncTimeJson != null) {
            try {
                syncTime = AppGsonBuilder.DEFAULT_FORMAT.fromJson(syncTimeJson, Date.class);
            } catch (JsonSyntaxException ignore) {
                getEditor().remove(PREFERENCE_EVENT_SYNCDATE).commit();
            }
        }

        return syncTime;
    }

    @Override
    public void setSyncTime(Date syncTime) {
        String syncTimeJson = AppGsonBuilder.DEFAULT_FORMAT.toJson(syncTime);
        if (syncTimeJson != null) {
            getEditor().putString(PREFERENCE_EVENT_SYNCDATE, syncTimeJson).commit();
        } else {
            getEditor().remove(PREFERENCE_EVENT_SYNCDATE);
        }
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return getContext().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

    protected static String createUdid(Context context) {
        byte[] bytes;
        String udid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (udid == null || udid.equals("9774d56d682e549c") || udid.length() < 15) {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                udid = telephonyManager.getDeviceId();
            } catch (Exception e) {
                SecureRandom random = new SecureRandom();
                udid = new BigInteger(64, random).toString(16);
            }
        }
        try {
            bytes = udid.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            bytes = udid.getBytes();
        }

        return UUID.nameUUIDFromBytes(bytes).toString();
    }
}
