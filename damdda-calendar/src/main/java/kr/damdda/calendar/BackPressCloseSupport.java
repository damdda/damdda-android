package kr.damdda.calendar;

import android.app.Activity;
import android.widget.Toast;

public class BackPressCloseSupport {
    public static final int DEFAULT_PRESSED_WAITING_TIME = 2000;
    private long mBackKeyPressedTime = 0;
    private int mPressedWaitingTime = DEFAULT_PRESSED_WAITING_TIME;
    private Toast mToast;

    private Activity mActivity;

    public BackPressCloseSupport(Activity activity) {
        this(activity, DEFAULT_PRESSED_WAITING_TIME);
    }

    public BackPressCloseSupport(Activity context, int pressedWaitingTime) {
        mActivity = context;
        mPressedWaitingTime = pressedWaitingTime;
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() > mBackKeyPressedTime + mPressedWaitingTime) {
            mBackKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }

        if (System.currentTimeMillis() <= mBackKeyPressedTime + mPressedWaitingTime) {
            mActivity.finish();
            mToast.cancel();
        }
    }

    protected void showGuide() {
        mToast = Toast.makeText(mActivity, "\'뒤로\'버튼을 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT);
        mToast.show();
    }
}
