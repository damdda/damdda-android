package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import kr.lethe.android.dao.SqliteType;
import kr.lethe.android.dao.annotation.Column;
import kr.lethe.android.dao.annotation.Id;
import kr.lethe.android.dao.annotation.Type;

public class User implements Serializable {
    @Id
    @Column(name = "id", type = @Type(SqliteType.UUID), primary = true)
    private UUID mId;

    @Column(name = "name", type = @Type(value = SqliteType.VARCHAR, size = 20), notnull = true)
    private String mName;

    @Column(name = "email", type = @Type(value = SqliteType.VARCHAR), notnull = true)
    private String mEmail;

    /**
     * 수정일시
     */
    @Column(name = "updated_time", type = @Type(SqliteType.DATETIME))
    private Date mUpdatedTime;

    /**
     * 생성일시
     */
    @Column(name = "created_time", type = @Type(SqliteType.DATETIME))
    private Date mCreatedTime;

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Date getUpdatedTime() {
        return mUpdatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        mUpdatedTime = updatedTime;
    }

    public Date getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(Date createdTime) {
        mCreatedTime = createdTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "mId='" + mId + '\'' +
                ", mName='" + mName + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mUpdatedTime='" + mUpdatedTime + '\'' +
                ", mCreatedTime='" + mCreatedTime + '\'' +
                '}';
    }
}
