package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.BaseModel;
import kr.lethe.android.dao.SqliteType;
import kr.lethe.android.dao.annotation.Column;
import kr.lethe.android.dao.annotation.Id;
import kr.lethe.android.dao.annotation.Table;
import kr.lethe.android.dao.annotation.Type;

@DebugLog
@Table(name="calendars")
public class Calendar extends BaseModel implements Serializable {
    /**
     * 캘린더 ID
     */
    @Id
    @Column(name = "id", type = @Type(SqliteType.UUID), primary = true)
    private UUID mId;

    /**
     * 상위 캘린더 ID
     */
    @Column(name = "parent", type = @Type(SqliteType.UUID), notnull = true)
    private UUID mParent;

    /**
     * 캘린더 소유자
     */
    @Column(name = "owner", type = @Type(SqliteType.UUID), notnull = true)
    private UUID mOwner;

    /**
     * 캘린더명
     */
    @Column(name = "name", type = @Type(SqliteType.VARCHAR), notnull = true)
    private String mName;

    /**
     * 설명
     */
    @Column(name = "description", type = @Type(SqliteType.VARCHAR))
    private String mDescription;

    /**
     * 캘린더색상
     */
    @Column(name = "color", type = @Type(value = SqliteType.VARCHAR, size = 15))
    private String mColor;

    /**
     * 캘린더 지역 (기준시간)
     */
    @Column(name = "local", type = @Type(value = SqliteType.CHAR, size = 5))
    private Locale mLocale;

    /**
     * 이벤트 갯수
     */
    @Column(name = "event_count", type = @Type(value = SqliteType.INTEGER), defaultValue="0", notnull = true)
    private int mEventCount;

    /**
     * 기본 캘린더 여부
     */
    @Column(name = "is_default", type = @Type(value = SqliteType.BOOLEAN), defaultValue = "0", notnull = true)
    private Boolean mIsDefault;

    /**
     * 시스템 캘린더 여부
     */
    @Column(name = "is_system", type = @Type(value = SqliteType.BOOLEAN), defaultValue = "0", notnull = true)
    private Boolean mIsSystem;

    /**
     * 정렬 순서
     */
    @Column(name = "order_index", type = @Type(value = SqliteType.INTEGER), defaultValue = "0", notnull = true)
    private int mOrderIndex;

    /**
     * 수정일시
     */
    @Column(name = "updated_time", type = @Type(SqliteType.DATETIME))
    private Date mUpdatedTime;

    /**
     * 생성일시
     */
    @Column(name = "created_time", type = @Type(SqliteType.DATETIME))
    private Date mCreatedTime;
}
