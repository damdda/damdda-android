package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;

public class AccessToken implements Serializable {
    private String mAccessToken;
    private String mTokenType;
    private String mRefreshToken;
    private Date mExpiresTime;

    public AccessToken(String accessToken) {
        this("Bearer", accessToken);
    }

    public AccessToken(String tokenType, String accessToken) {
        this(tokenType, accessToken, null);
    }

    public AccessToken(String tokenType, String accessToken, String refreshToken) {
        this.mTokenType = tokenType;
        this.mAccessToken = accessToken;
        this.mRefreshToken = refreshToken;
    }

    public AccessToken(String accessToken, long expiresIn) {
        this("Bearer", accessToken, expiresIn);
    }

    public AccessToken(String tokenType, String accessToken, long expiresIn) {
        this.mAccessToken = accessToken;
        this.mTokenType = tokenType;
        this.mExpiresTime = new Date(System.currentTimeMillis() + expiresIn);
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setTokenType(String tokenType) {
        mTokenType = tokenType;
    }

    public String getTokenType() {
        return mTokenType;
    }

    public void setRefreshToken(String refreshToken) {
        mRefreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }

    public void setExpiresTime(Date expiresTime) {
        mExpiresTime = expiresTime;
    }

    public Date getExpiresTime() {
        return mExpiresTime;
    }


    public boolean isExpired() {
        if (mExpiresTime == null) return true;
        return new Date().after(mExpiresTime);
    }

    @Override
    public String toString() {
        return String.format("%s %s", getTokenType(), getAccessToken());
    }
}