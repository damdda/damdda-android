package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import hugo.weaving.DebugLog;
import kr.lethe.android.dao.BaseModel;
import kr.lethe.android.dao.SqliteConverter;
import kr.lethe.android.dao.SqliteType;
import kr.lethe.android.dao.annotation.Column;
import kr.lethe.android.dao.annotation.Converter;
import kr.lethe.android.dao.annotation.Id;
import kr.lethe.android.dao.annotation.Table;
import kr.lethe.android.dao.annotation.Type;

@DebugLog
@Table(name = "alarm")
public class Alarm extends BaseModel implements Serializable {
    /**
     * 알람 ID
     */
    @Id
    @Column(name = "id", type = @Type(SqliteType.UUID), primary = true)
    private UUID mId;
    /**
     * 알람 포인트 ID
     */
    @Column(name = "target", type = @Type(SqliteType.UUID))
    private UUID mTarget;

    /**
     * 알람 포인트 타입
     */
    @Column(name = "target_type", type = @Type(value = SqliteType.VARCHAR, size = 20))
    private String mTargetType;

    /**
     * 알림
     */
    @Column(name = "trigger", type = @Type(value = SqliteType.VARCHAR, size = 30))
    private String mTrigger;

    /**
     * 알림
     */
    @Column(name = "trigger_time", type = @Type(value = SqliteType.DATETIME))
    private Date mTriggerTime;

    /**
     * 알람 타입
     */
    @Column(name = "action", type = @Type(value = SqliteType.VARCHAR, size = 30))
    @Converter(SqliteConverter.SET_COMMA)
    private Set<String> mAction;

    /**
     * 알람 설명
     */
    @Column(name = "description", type = @Type(SqliteType.VARCHAR))
    private String mDescription;

    /**
     * 알람 타입이 EMAIL일 경우 발송할 이메일 주소
     */
    @Column(name = "email", type = @Type(value = SqliteType.VARCHAR, size = 50))
    private String mEmail;

    /**
     * 알람 타입이 AUDIO일 경우 사운드
     */
    @Column(name = "sound", type = @Type(value = SqliteType.VARCHAR, size = 50))
    private String mSound;

    /**
     * 수정일시
     */
    @Column(name = "updated_time", type = @Type(SqliteType.DATETIME))
    private Date mUpdatedTime;

    /**
     * 생성일시
     */
    @Column(name = "created_time", type = @Type(SqliteType.DATETIME))
    private Date mCreatedTime;

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public UUID getTarget() {
        return mTarget;
    }

    public void setTarget(UUID target) {
        mTarget = target;
    }

    public String getTargetType() {
        return mTargetType;
    }

    public void setTargetType(String targetType) {
        mTargetType = targetType;
    }

    public String getTrigger() {
        return mTrigger;
    }

    public void setTrigger(String trigger) {
        mTrigger = trigger;
    }

    public Date getTriggerTime() {
        return mTriggerTime;
    }

    public void setTriggerTime(Date triggerTime) {
        mTriggerTime = triggerTime;
    }

    public Set<String> getAction() {
        return mAction;
    }

    public void setAction(Set<String> action) {
        mAction = action;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getSound() {
        return mSound;
    }

    public void setSound(String sound) {
        mSound = sound;
    }

    public Date getUpdatedTime() {
        return mUpdatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        mUpdatedTime = updatedTime;
    }

    public Date getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(Date createdTime) {
        mCreatedTime = createdTime;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "mId=" + mId +
                ", mTarget=" + mTarget +
                ", mTargetType='" + mTargetType + '\'' +
                ", mTrigger='" + mTrigger + '\'' +
                ", mTriggerTime=" + mTriggerTime +
                ", mAction=" + mAction +
                ", mDescription='" + mDescription + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mSound='" + mSound + '\'' +
                ", mUpdatedTime=" + mUpdatedTime +
                ", mCreatedTime=" + mCreatedTime +
                '}';
    }
}
