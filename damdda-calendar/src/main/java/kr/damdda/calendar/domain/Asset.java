package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import kr.lethe.android.dao.BaseModel;

public class Asset extends BaseModel implements Serializable {
    /**
     * 첨부파일 ID
     */
    private UUID mId;

    /**
     * 첨부파일 소유자
     */
    private UUID mOwner;

    /**
     * 부모 첨부파일 ID
     */
    private UUID mParent;

    /**
     * 첨부파일 포인트 ID
     */
    private UUID mTarget;

    /**
     * 첨부파일 포인트 타입
     */
    private TargetType mTargetType;

    /**
     * 파일명
     */
    private String mName;

    /**
     * 파일경로
     */
    private String mPath;

    /**
     * MIME 타입
     */
    private String mMime;

    /**
     * 파일크기
     */
    private long mSize;

    /**
     * 가로크기
     */
    private int mWidth;

    /**
     * 세로크기
     */
    private int mHeight;

    /**
     * 수정일시
     */
    private Date mUpdatedTime;

    /**
     * 생성일시
     */
    private Date mCreatedTime;

    public static enum TargetType {
        PROFILE, EVENT
    }
}
