package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Page<T> implements Serializable {
    private List<T> mData = new ArrayList<T>();
    private int mCount = 0;

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public List<T> getData() {
        return mData;
    }

    public void setData(List<T> data) {
        mData = data;
    }

    @Override
    public String toString() {
        return "Page{" +
                "mData=" + mData +
                ", mCount=" + mCount +
                '}';
    }
}
