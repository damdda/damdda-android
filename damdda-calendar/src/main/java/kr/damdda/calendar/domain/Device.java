package kr.damdda.calendar.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Device implements Serializable {
    private UUID mId;
    private UUID mOwner;
    private String mPlatform;
    private String mDeviceToken;
    private String mState;
    private Date mUpdatedTime;
    private Date mCreatedTime;

    public Device(String deviceToken) {
        mPlatform = "ANDROID";
        mDeviceToken = deviceToken;
        mState = "OK";
    }

    public UUID getId() {
        return mId;
    }

    public UUID getOwner() {
        return mOwner;
    }

    public String getPlatform() {
        return mPlatform;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Date getUpdatedTime() {
        return mUpdatedTime;
    }

    public Date getCreatedTime() {
        return mCreatedTime;
    }
}
