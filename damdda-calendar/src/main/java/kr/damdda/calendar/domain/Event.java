package kr.damdda.calendar.domain;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import kr.lethe.android.dao.BaseModel;
import kr.lethe.android.dao.SqliteDao;
import kr.lethe.android.dao.SqliteType;
import kr.lethe.android.dao.annotation.Column;
import kr.lethe.android.dao.annotation.Id;
import kr.lethe.android.dao.annotation.Table;
import kr.lethe.android.dao.annotation.Type;

@Table(name = "events")
public class Event extends BaseModel implements Serializable {
    @Id
    @Column(name = "id", type = @Type(SqliteType.UUID), primary = true)
    private UUID mId;

    @Column(name = "parent", type = @Type(SqliteType.UUID))
    private UUID mParent;

    @Column(name = "owner", type = @Type(SqliteType.UUID), notnull = true)
    private UUID mOwner;

    @Column(name = "calendar", type = @Type(SqliteType.UUID), notnull = true)
    private UUID mCalendar;

    @Column(name = "name", type = @Type(value = SqliteType.VARCHAR, size = 50), notnull = true)
    private String mName;

    @Column(name = "description", type = @Type(SqliteType.TEXT))
    private String mDescription;

    @Column(name = "start_time", type = @Type(SqliteType.DATETIME), notnull = true)
    private Date mStartTime;

    @Column(name = "end_time", type = @Type(SqliteType.DATETIME), notnull = true)
    private Date mEndTime;

    @Column(name = "is_nunar", type = @Type(SqliteType.BOOLEAN))
    private boolean mIsLunar;

    @Column(name = "classify", type = @Type(value = SqliteType.VARCHAR, size = 20))
    private Classify mClassify;

    @Column(name = "location", type = @Type(SqliteType.VARCHAR))
    private String mLocation;

    @Column(name = "latitude", type = @Type(SqliteType.REAL))
    private double mLatitude;

    @Column(name = "longitude", type = @Type(SqliteType.REAL))
    private double mLongitude;

//    @Column(name = "attendee", type = @Type(SqliteType.VARCHAR))
//    @Converter(SqliteConverter.LIST_COMMA)
//    private List<String> mAttendee = new ArrayList<String>();

//    @Column(name = "recur_rule", type = @Type(SqliteType.VARCHAR))
//    private RecurRule mRecurRule;

    @Column(name = "sticker", type = @Type(SqliteType.VARCHAR))
    private String mSticker;

    @Column(name = "color", type = @Type(value = SqliteType.VARCHAR, size = 15))
    private String mColor;

    @Column(name = "calendar_color", type = @Type(value = SqliteType.VARCHAR, size = 15))
    private String mCalendarColor;

    @Column(name = "url", type = @Type(SqliteType.VARCHAR))
    private String mUrl;

    @Column(name = "poster", type = @Type(SqliteType.VARCHAR))
    private String mPosterUrl;

    @Column(name = "is_damdda", type = @Type(SqliteType.BOOLEAN), notnull = true, defaultValue = "0")
    private boolean mIsDamdda;

    @Column(name = "alarm_count", type = @Type(SqliteType.INTEGER), notnull = true, defaultValue = "0")
    private int mAlarmCount;

    @Column(name = "attach_count", type = @Type(SqliteType.INTEGER), notnull = true, defaultValue = "0")
    private int mAttachCount;

    @Column(name = "like_count", type = @Type(SqliteType.LONG), notnull = true, defaultValue = "0")
    private long mLikeCount;

    @Column(name = "damdda_count", type = @Type(SqliteType.LONG), notnull = true, defaultValue = "0")
    private long mDamddaCount;

    @Column(name = "comment_count", type = @Type(SqliteType.LONG), notnull = true, defaultValue = "0")
    private long mCommentCount;

    @Column(name = "view_count", type = @Type(SqliteType.LONG), notnull = true, defaultValue = "0")
    private long mViewCount;

    @Column(name = "updated_time", type = @Type(SqliteType.DATETIME))
    private Date mUpdatedTime;

    @Column(name = "created_time", type = @Type(SqliteType.DATETIME))
    private Date mCreatedTime;

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public UUID getParent() {
        return mParent;
    }

    public void setParent(UUID parent) {
        mParent = parent;
    }

    public UUID getOwner() {
        return mOwner;
    }

    public void setOwner(UUID owner) {
        mOwner = owner;
    }

    public UUID getCalendar() {
        return mCalendar;
    }

    public void setCalendar(UUID calendar) {
        mCalendar = calendar;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Date getStartTime() {
        return mStartTime;
    }

    public void setStartTime(Date startTime) {
        mStartTime = startTime;
    }

    public Date getEndTime() {
        return mEndTime;
    }

    public void setEndTime(Date endTime) {
        mEndTime = endTime;
    }

    public boolean isLunar() {
        return mIsLunar;
    }

    public void setLunar(boolean isLunar) {
        mIsLunar = isLunar;
    }

    public Classify getClassify() {
        return mClassify;
    }

    public void setClassify(Classify classify) {
        mClassify = classify;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }
//
//    public List<String> getAttendee() {
//        return mAttendee;
//    }
//
//    public void setAttendee(List<String> attendee) {
//        mAttendee = attendee;
//    }
//
//    public RecurRule getRecurRule() {
//        return mRecurRule;
//    }
//
//    public void setRecurRule(RecurRule recurRule) {
//        mRecurRule = recurRule;
//    }

    public String getSticker() {
        return mSticker;
    }

    public void setSticker(String sticker) {
        mSticker = sticker;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        mColor = color;
    }

    public String getCalendarColor() {
        return mCalendarColor;
    }

    public void setCalendarColor(String calendarColor) {
        mCalendarColor = calendarColor;
    }

    public String getComputedColor(){
        if(mColor == null || mColor.isEmpty()){
            return mCalendarColor;
        }else{
            return mColor;
        }
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getPosterUrl() {
        return mPosterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        mPosterUrl = posterUrl;
    }

    public boolean isDamdda() {
        return mIsDamdda;
    }

    public void setDamdda(boolean isDamdda) {
        mIsDamdda = isDamdda;
    }

    public int getAlarmCount() {
        return mAlarmCount;
    }

    public void setAlarmCount(int alarmCount) {
        mAlarmCount = alarmCount;
    }

    public int getAttachCount() {
        return mAttachCount;
    }

    public void setAttachCount(int attachCount) {
        mAttachCount = attachCount;
    }

    public long getLikeCount() {
        return mLikeCount;
    }

    public void setLikeCount(long likeCount) {
        mLikeCount = likeCount;
    }

    public long getDamddaCount() {
        return mDamddaCount;
    }

    public void setDamddaCount(long damddaCount) {
        mDamddaCount = damddaCount;
    }

    public long getCommentCount() {
        return mCommentCount;
    }

    public void setCommentCount(long commentCount) {
        mCommentCount = commentCount;
    }

    public long getViewCount() {
        return mViewCount;
    }

    public void setViewCount(long viewCount) {
        mViewCount = viewCount;
    }

    public Date getUpdatedTime() {
        return mUpdatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        mUpdatedTime = updatedTime;
    }

    public Date getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(Date createdTime) {
        mCreatedTime = createdTime;
    }

    public String getPeriod() {
        return getPeriod("yyyy.MM.dd");
    }

    public String getPeriod(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return String.format("%s ~ %s", dateFormat.format(getStartTime()), dateFormat.format(getEndTime()));
    }

    public List<Alarm> getAlarms(SqliteDao alarmSqliteDao){
        return alarmSqliteDao.query("target=? and target_type='EVENT'", this.getId().toString()).asList();
    }

    @Override
    public String toString() {
        return "Event{" +
                "mId=" + mId +
                ", mParent=" + mParent +
                ", mOwner=" + mOwner +
                ", mCalendar=" + mCalendar +
                ", mName='" + mName + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mStartTime=" + mStartTime +
                ", mEndTime=" + mEndTime +
                ", mIsLunar=" + mIsLunar +
                ", mClassify=" + mClassify +
                ", mLocation='" + mLocation + '\'' +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                ", mSticker='" + mSticker + '\'' +
                ", mColor='" + mColor + '\'' +
                ", mCalendarColor='" + mCalendarColor + '\'' +
                ", mUrl='" + mUrl + '\'' +
                ", mPosterUrl='" + mPosterUrl + '\'' +
                ", mIsDamdda=" + mIsDamdda +
                ", mAlarmCount=" + mAlarmCount +
                ", mAttachCount=" + mAttachCount +
                ", mLikeCount=" + mLikeCount +
                ", mDamddaCount=" + mDamddaCount +
                ", mCommentCount=" + mCommentCount +
                ", mViewCount=" + mViewCount +
                ", mUpdatedTime=" + mUpdatedTime +
                ", mCreatedTime=" + mCreatedTime +
                '}';
    }

    public static enum Classify implements Serializable {
        PUBLIC, PRIVATE, CONFIDENTIAL, FRIENDS
    }

    public static class RecurRule implements Serializable {
        private Frequency mFreq;
        private Date mUntil;
        private int mCount;
        private int mInterval;
        private Weekday mWeekStart;
        private Set<Integer> mBySetPos;
        private Set<Integer> mByMonth;
        private Set<Integer> mByWeekNo;
        private Set<Integer> mByYearDay;
        private Set<Integer> mByMonthDay;
        private Set<Weekday> mByDay;
        private Set<Integer> mByHour;
        private Set<Integer> mByMinute;
        private Set<Integer> mBySecond;
    }

    public static enum Frequency implements Serializable {
        YEARLY, MONTHLY, WEEKLY, DAILY, HOURLY, MINUTELY, SECONDLY
    }

    public static enum Weekday implements Serializable {
        SU, MO, TU, WE, TH, FR, SA
    }
}
