package kr.damdda.calendar.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.Application;
import kr.damdda.calendar.BackPressCloseSupport;
import kr.damdda.calendar.GcmSupport;
import kr.damdda.calendar.R;
import kr.damdda.calendar.dao.AlarmSqliteDao;
import kr.damdda.calendar.dao.CalendarSqliteDao;
import kr.damdda.calendar.dao.EventSqliteDao;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.event.ChangedEventMessage;
import kr.damdda.calendar.event.CreatedEventMessage;
import kr.damdda.calendar.event.DeletedEventMessage;
import kr.damdda.calendar.event.LoggedMessage;
import kr.damdda.calendar.event.LogoutMessage;
import kr.damdda.calendar.event.RejectMessage;
import kr.damdda.calendar.service.WakefulAlarmListener;
import kr.damdda.calendar.service.WakefulAlarmService;
import kr.damdda.calendar.widget.Dialog;
import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.google.android.gms.analytics.Tracker;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaWebView;
import org.xwalk.core.XWalkView;
import org.xwalk.core.internal.XWalkSettings;
import org.xwalk.core.internal.XWalkViewBridge;

import java.text.SimpleDateFormat;
import java.util.UUID;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import kr.lethe.android.async.OnAsyncTaskListenerImpl;
import kr.lethe.android.dao.SqliteDao;
import kr.lethe.android.util.ContextUtils;
import kr.lethe.android.util.ReflectUtils;

@DebugLog
public class MainActivity extends CordovaActivity {
    /**
     * 참고 ID 전달용 식별자
     */
    public static final String EXTRA_REFERENCE_ID = "MAIN_EXTRA_REFERENCE_ID";
    /**
     * 전달된 Extra에 대한 실행방법
     */
    public static final String EXTRA_EXEC_TYPE = "MAIN_EXTRA_EXEC_TYPE";
    /**
     * 이벤트 상세보기
     */
    public static final String EXEC_TYPE_EVENT_DETAIL = "EXEC_TYPE_EVENT_DETAIL";
    /**
     * 이벤트 수정
     */
    public static final String EXEC_TYPE_EVENT_EDIT = "EXEC_TYPE_EVENT_EDIT";
    /**
     * 이벤트 추가
     */
    public static final String EXEC_TYPE_EVENT_ADD = "EXEC_TYPE_EVENT_ADD";
    /**
     * 이벤트 댓글보기
     */
    public static final String EXEC_TYPE_EVENT_COMMENT = "EXEC_TYPE_EVENT_COMMENT";
    /**
     * 프로필로 이동
     */
    public static final String EXEC_TYPE_PROFILE = "EXEC_TYPE_PROFILE";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

    private static final String TAG = MainActivity.class.getSimpleName();
    private AppContext mAppContext;
    private GcmSupport mGcmSupport;
    private XWalkSettings mXWalkSettings = null;
    private BackPressCloseSupport mBackPressCloseSupport = null;
    private EventSqliteDao mEventSqliteDao;
    private Dialog mDialog;


    @DebugLog
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppContext = AppContextFactory.getInstance(this);
        mGcmSupport = new GcmSupport(this);
        mBackPressCloseSupport = new BackPressCloseSupport(this);
        mEventSqliteDao = new EventSqliteDao(mAppContext.getDatabaseHelper());

        if(this.isNetworkConnected()) {
            loadUrl(launchUrl);
        }
        parseIntent(getIntent());

        WakefulIntentService.scheduleAlarms(new WakefulAlarmListener(), this, false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        parseIntent(getIntent());
    }

    public void parseIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            loadUrl(data.toString());
        } else {
            parseExtra(intent.getExtras());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!this.isNetworkConnected()) {
            checkNetwork();
        }
    }

    public void parseExtra(Bundle extra) {
        if (extra == null) return;

        String eventIdString = extra.getString(MainActivity.EXTRA_REFERENCE_ID);
        if(eventIdString == null) return;

        String type = extra.getString(MainActivity.EXTRA_EXEC_TYPE);
        if (type == null) return;

        UUID referenceId = UUID.fromString(eventIdString);
        switch (type) {
            case MainActivity.EXEC_TYPE_EVENT_DETAIL:
                if (referenceId == null) return;
                loadUrl(String.format("%s/#/events/%s", launchUrl, referenceId));
                break;
            case MainActivity.EXEC_TYPE_EVENT_EDIT:
                if (referenceId == null) return;
                loadUrl(String.format("%s/#/events/%s/edit", launchUrl, referenceId));
                break;
            case MainActivity.EXEC_TYPE_EVENT_ADD:
                if (referenceId == null) return;
                loadUrl(String.format("%s/#/events/new/%s", launchUrl, referenceId));
                break;
            case MainActivity.EXEC_TYPE_EVENT_COMMENT:
                if (referenceId == null) return;
                loadUrl(String.format("%s/#/events/%s/comments", launchUrl, referenceId));
                break;
            case MainActivity.EXEC_TYPE_PROFILE:
                if (referenceId == null) return;
                loadUrl(String.format("%s/#/profile/%s", launchUrl, referenceId));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        mBackPressCloseSupport.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Tracker tracker = ((Application)getApplication()).getTracker(Application.TrackerName.APP_TRACKER);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mAppContext.getDatabaseHelper().close();
    }

    @Override
    protected CordovaWebView makeWebView() {
        CordovaWebView webView = super.makeWebView();

        String versionName = "unknown";
        try {
            PackageInfo packageInfo = ContextUtils.getPackageInfo(this);
            versionName = packageInfo.versionName;
        } catch (Exception e) {
        }

        if (webView.getView() instanceof XWalkView) {
            XWalkView xWalkView = (XWalkView) webView.getView();
            getXWalkSettings(xWalkView).setAppCacheEnabled(true);
            getXWalkSettings(xWalkView).setAppCachePath(String.format("%s/appcache", getExternalCacheDir().getAbsolutePath()));

            String userAgent = getXWalkSettings(xWalkView).getUserAgentString();
            userAgent = String.format("%s Cordova/%s %s/%s", userAgent, CordovaWebView.CORDOVA_VERSION, getString(R.string.user_agent), versionName);
            getXWalkSettings(xWalkView).setUserAgentString(userAgent);
        }

        return webView;
    }

    private XWalkSettings getXWalkSettings(XWalkView webView) {
        if (mXWalkSettings == null) {
            Object bridge = ReflectUtils.invokeMethod(XWalkView.class, webView, "getBridge");
            if (bridge instanceof XWalkViewBridge) {
                XWalkViewBridge xWalkViewBridge = (XWalkViewBridge) bridge;
                mXWalkSettings = xWalkViewBridge.getSettings();
            }
        }

        return mXWalkSettings;
    }

    public void onEvent(LoggedMessage eventMessage) {
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (mGcmSupport.checkPlayServices()) {
            if (mAppContext.getRegistrationId() == null) {
                mGcmSupport.registerInBackground(mAppContext.getSenderId(), new OnAsyncTaskListenerImpl<String, Void, String>() {
                    @Override
                    public void onComplete(AsyncTask<String, Void, String> sender, String registrationId) {
                        mAppContext.saveRegistrationId(registrationId);
                    }
                });
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
        clearDatabase();
        mAppContext.setUser(eventMessage.getUser());
        WakefulAlarmService.syncEvents(this);
    }

    public void onEvent(RejectMessage eventMessage) {
        clearUserData();
        clearDatabase();
    }

    public void onEvent(LogoutMessage eventMessage) {
        clearUserData();
        clearDatabase();
    }

    public void onEvent(CreatedEventMessage eventMessage) {
        Event event = eventMessage.getEvent();
        if (event == null) return;
        try {
            WakefulAlarmService.syncEvents(this);
        }catch (Exception e){
        }
    }

    public void onEvent(ChangedEventMessage eventMessage) {
        Event event = eventMessage.getEvent();
        if (event == null) return;
        try {
            WakefulAlarmService.syncEvents(this);
        }catch (Exception e){
        }

    }

    public void onEvent(DeletedEventMessage eventMessage) {
        Event event = eventMessage.getEvent();
        if (event == null) return;
        try {
            event.setSqliteDao(mEventSqliteDao);
            event.delete();
        }catch(Exception ignore){
        }
        try {
            WakefulAlarmService.syncEvents(this);
        }catch (Exception e){
        }
    }

    private void clearUserData() {
        try {
            mAppContext.setUser(null);
            mAppContext.setDevice(null);
            mAppContext.setSyncTime(null);
            mAppContext.saveRegistrationId(null);
        }catch(Exception e){
        }
    }

    private void clearDatabase() {
        try {
            SqliteDao eventSqliteDao = new EventSqliteDao(mAppContext.getDatabaseHelper());
            eventSqliteDao.deleteAll();

            SqliteDao calendarSqliteDao = new CalendarSqliteDao(mAppContext.getDatabaseHelper());
            calendarSqliteDao.deleteAll();

            SqliteDao alarmSqliteDao = new AlarmSqliteDao(mAppContext.getDatabaseHelper());
            alarmSqliteDao.deleteAll();
        }catch(Exception e){
        }
    }

    private boolean isNetworkConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected() || cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    private void checkNetwork() {
        // 네트워크 사용 가능할 경우 업데이트
        if (!this.isNetworkConnected()) {
            if (mDialog != null) {
                mDialog.dismiss();
            }

            mDialog = new Dialog(this);
            mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            });
            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (mDialog != null) {
                        mDialog = null;
                    }
                }
            });
            mDialog.setContentView(getResources().getString(R.string.msg_not_use_network));
            mDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mDialog.setCommandView(R.layout.dialog_command_not_network_setting);
            View settingButton = mDialog.getCommand().findViewById(R.id.setting);
            if (settingButton != null) {
                settingButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivity(intent);
                    }
                });
            }

            View closeButton = mDialog.getCommand().findViewById(R.id.close);
            if (closeButton != null) {
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }

            mDialog.show();
        }
    }
}
