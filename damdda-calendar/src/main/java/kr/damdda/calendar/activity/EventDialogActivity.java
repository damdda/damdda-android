package kr.damdda.calendar.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.widget.EventDialog;

import java.io.Serializable;

public class EventDialogActivity extends Activity {
    public static final String EXTRA_EVENT = "EVENT";

    private static Dialog sEventDialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        if (extra != null) {
            Serializable event = extra.getSerializable(EXTRA_EVENT);

            if (event instanceof Event) {
                showDialog((Event) event, new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                        overridePendingTransition(0, 0);
                    }
                });
            } else {
                finish();
                overridePendingTransition(0, 0);
            }
        }
    }

    public void showDialog(Event event, DialogInterface.OnDismissListener dismissListener) {
        if (sEventDialog != null) sEventDialog.dismiss();

        sEventDialog = new EventDialog(this, event);
        sEventDialog.setOnDismissListener(dismissListener);
        sEventDialog.show();
    }
}
