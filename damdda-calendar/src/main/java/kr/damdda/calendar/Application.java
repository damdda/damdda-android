package kr.damdda.calendar;

import android.content.Context;

import kr.damdda.calendar.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

public class Application extends android.app.Application implements Thread.UncaughtExceptionHandler{

    // The following line should be changed to include the correct property id.
    private static final String PROPERTY_ID = "UA-62257361-1";

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER // Tracker used by all the apps from a company. eg: roll-up tracking.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            if (trackerId == TrackerName.APP_TRACKER) {
                mTrackers.put(trackerId, analytics.newTracker(PROPERTY_ID));
            } else if (trackerId == TrackerName.GLOBAL_TRACKER) {
                mTrackers.put(trackerId, analytics.newTracker(R.xml.global_tracker));
            }
        }
        return mTrackers.get(trackerId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable e) {
        try {
            e.printStackTrace();

            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }catch(Exception ignore){
        }
    }
}
