package kr.damdda.calendar.protocol;


public class ProtocolFactory {
    public static Protocol create(String apiUrl) {
        return new RestfulProtocol(apiUrl);
    }
}
