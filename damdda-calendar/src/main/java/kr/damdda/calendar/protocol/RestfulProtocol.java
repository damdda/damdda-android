package kr.damdda.calendar.protocol;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hugo.weaving.DebugLog;
import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.Page;
import kr.damdda.calendar.domain.User;
import kr.damdda.calendar.json.AppGsonBuilder;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.Converter;

@DebugLog
public class RestfulProtocol implements Protocol {
    private static final String TAG = RestfulProtocol.class.getSimpleName();
    private final RestAdapter mRestAdapter;
    private final RestfulService mRestfulService;
    private AccessToken mAccessToken;
    private Map<String, String> mHeaders = new HashMap<String, String>();
    private Map<String, String> mPathParams = new HashMap<String, String>();
    private Map<String, String> mQueryParams = new HashMap<String, String>();

    public RestfulProtocol(String url) {
        Converter converter = new GsonConverter(AppGsonBuilder.DEFAULT);
        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(converter)
                .setRequestInterceptor(requestInterceptor())
                .build();

        mRestfulService = mRestAdapter.create(RestfulService.class);
    }

    protected RequestInterceptor requestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                for (Map.Entry<String, String> entry : mHeaders.entrySet()) {
                    request.addHeader(entry.getKey(), entry.getValue());
                }

                for (Map.Entry<String, String> entry : mPathParams.entrySet()) {
                    request.addPathParam(entry.getKey(), entry.getValue());
                }

                for (Map.Entry<String, String> entry : mQueryParams.entrySet()) {
                    request.addQueryParam(entry.getKey(), entry.getValue());
                }
            }
        };
    }

    public void setAccessToken(AccessToken accessToken) {
        mAccessToken = accessToken;
    }

    @Override
    public void addHeader(String name, String value) {
        Pattern accessTokenPattern = Pattern.compile("(\\w+)\\s([^\\s]+)");
        if (name.equalsIgnoreCase(HEADER_AUTHORIZATION)) {
            if (value == null) {
                setAccessToken(null);
            } else {
                Matcher matcher = accessTokenPattern.matcher(value);
                if (matcher.find()) {
                    setAccessToken(new AccessToken(matcher.group(1), matcher.group(2)));
                    return;
                }
            }
        }
        if (value == null) {
            mHeaders.remove(name);
        } else {
            mHeaders.put(name, value);
        }
    }

    @Override
    public void addPathParam(String name, String value) {
        if(value != null) {
            mPathParams.put(name, value);
        }else{
            mPathParams.remove(name);
        }
    }

    @Override
    public void addQueryParam(String name, String value) {
        if(value != null) {
            mQueryParams.put(name, value);
        }else{
            mQueryParams.remove(name);
        }
    }

    @Override
    public AccessToken refreshToken(String clientId, String refreshToken) {
        return mRestfulService.refreshToken(clientId, refreshToken);
    }

    @Override
    public Device registerDevice(Device device) {
        assert mAccessToken != null;

        return mRestfulService.registerDevice(mAccessToken.toString(), device);
    }

    @Override
    public User getMe() {
        assert mAccessToken != null;

        return mRestfulService.getUser(mAccessToken.toString());
    }

    @Override
    public User getUserInfo(UUID userId) {
        assert mAccessToken != null;

        return mRestfulService.getUser(mAccessToken.toString(), userId);
    }

    @Override
    public Page<Event> getEvents(Map<String, String> options) {
        assert mAccessToken != null;

        return mRestfulService.getEvents(mAccessToken.toString(), options);
    }

    @Override
    public Event getEvent(UUID eventId) {
        assert mAccessToken != null;

        return mRestfulService.getEvent(mAccessToken.toString(), eventId);
    }

    @Override
    public Page<Calendar> getCalendars(Map<String, String> options) {
        assert mAccessToken != null;

        return mRestfulService.getCalendars(mAccessToken.toString(), options);
    }

    @Override
    public Calendar getCalendar(UUID calendarId) {
        assert mAccessToken != null;

        return mRestfulService.getCalendar(mAccessToken.toString(), calendarId);
    }

    @Override
    public Page<Alarm> getAlarms(Map<String, String> options) {
        assert mAccessToken != null;

        return mRestfulService.getAlarms(mAccessToken.toString(), options);
    }

    @Override
    public Alarm getAlarm(UUID alarmId) {
        assert mAccessToken != null;

        return mRestfulService.getAlarm(mAccessToken.toString(), alarmId);
    }
}
