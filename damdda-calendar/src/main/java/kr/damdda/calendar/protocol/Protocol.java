package kr.damdda.calendar.protocol;

import java.util.Map;
import java.util.UUID;

import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.Page;
import kr.damdda.calendar.domain.User;

public interface Protocol {
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_CONTENT_LENGTH = "Content-Length";
    public static final String HEADER_ACCEPT = "Accept";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String MIME_JSON = "application/json";

    public void addHeader(String name, String value);

    public void addPathParam(String name, String value);

    public void addQueryParam(String name, String value);

    public AccessToken refreshToken(String clientId, String refreshToken);

    public Device registerDevice(Device device);

    public User getMe();

    public User getUserInfo(UUID userId);

    public Page<Event> getEvents(Map<String, String> options);

    public Event getEvent(UUID eventId);

    public Page<Calendar> getCalendars(Map<String, String> options);

    public Calendar getCalendar(UUID calendarId);

    public Page<Alarm> getAlarms(Map<String, String> options);

    public Alarm getAlarm(UUID alarmId);
}
