package kr.damdda.calendar.protocol;

import java.util.Map;
import java.util.UUID;

import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.Page;
import kr.damdda.calendar.domain.User;
import retrofit.http.*;

public interface RestfulService {
    @POST("/oauth/token")
    public AccessToken refreshToken(String clientId, String refreshToken);

    @POST("/devices")
    public Device registerDevice(@Header("Authorization") String authorization, @Body Device device);

    @DELETE("/devices/{id}")
    public void unregisterDevice(@Header("Authorization") String authorization, @Path("id") UUID deviceId);

    @GET("/events")
    public Page<Event> getEvents(@Header("Authorization") String authorization, @QueryMap Map<String, String> options);

    @GET("/events/{id}")
    public Event getEvent(@Header("Authorization") String authorization, @Path("id") UUID eventId);

    @GET("/calendars")
    public Page<Calendar> getCalendars(@Header("Authorization") String authorization, @QueryMap Map<String, String> options);

    @GET("/calendars/{id}")
    public Calendar getCalendar(@Header("Authorization") String authorization, @Path("id") UUID calenderId);

    @GET("/alarms")
    public Page<Alarm> getAlarms(@Header("Authorization") String authorization, @QueryMap Map<String, String> options);

    @GET("/alarms/{id}")
    public Alarm getAlarm(@Header("Authorization") String authorization, @Path("id") UUID alarmId);

    @GET("/users/{id}")
    public User getUser(@Header("Authorization") String authorization, @Path("id") UUID userId);

    @GET("/users/me")
    public User getUser(@Header("Authorization") String authorization);
}
