package kr.damdda.calendar.protocol;

import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.lang.reflect.Type;

import hugo.weaving.DebugLog;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

@DebugLog
public class GsonConverter implements Converter {
    private static final String MIME = "application/json";
    private final Gson mGson;
    private String mCharset = "UTF-8";

    public GsonConverter(Gson gson) {
        mGson = gson;
    }

    public GsonConverter(Gson gson, String charset) {
        mGson = gson;
        mCharset = charset;
    }

    @Override
    public Object fromBody(TypedInput body, Type type) throws ConversionException {
        try {
            InputStreamReader reader = new InputStreamReader(body.in(), mCharset);
            return mGson.fromJson(reader, type);
        } catch (Exception e) {
            throw new ConversionException(e);
        }
    }

    @Override
    public TypedOutput toBody(Object object) {
        return new TypedByteArray(String.format("%s; charset=%s", MIME, mCharset), mGson.toJson(object).getBytes());
    }
}
