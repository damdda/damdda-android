package kr.damdda.calendar;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.List;

import hugo.weaving.DebugLog;
import kr.lethe.android.receiver.NetworkChangeReceiver;
import kr.lethe.android.receiver.OnNetworkChangeListener;
import kr.lethe.android.receiver.Receiver;

@DebugLog
public class AppService extends Service {
    private static final String TAG = AppService.class.getSimpleName();
    private List<Receiver> mReceivers = new ArrayList<Receiver>();
    private NetworkChangeReceiver mNetworkChangeReceiver = new NetworkChangeReceiver(this);

    @Override
    public void onCreate() {
        super.onCreate();

        mReceivers.add(mNetworkChangeReceiver);

        for (Receiver receiver : mReceivers) {
            receiver.registerReceiver();
        }
    }

    @Override
    public void onDestroy() {
        for (Receiver receiver : mReceivers) {
            receiver.unregisterReceiver();
        }
        super.onDestroy();
    }

    public void setOnNetworkChangeListener(OnNetworkChangeListener onNetworkChangeListener) {
        mNetworkChangeReceiver.setOnNetworkChangeListener(onNetworkChangeListener);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
