package kr.damdda.calendar;

import java.util.Map;
import java.util.UUID;

import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.Page;
import kr.damdda.calendar.domain.User;
import kr.damdda.calendar.protocol.Protocol;

public class AppProtocol implements Protocol {
    private static final String TAG = AppProtocol.class.getSimpleName();

    private static AppProtocol sInstance = null;

    private Protocol mDelegateProtocol;

    public static AppProtocol getInstance(Protocol protocol) {
        if (sInstance == null) {
            sInstance = new AppProtocol(protocol);
        }

        sInstance.mDelegateProtocol = protocol;

        return sInstance;
    }

    private AppProtocol(Protocol protocol) {
        mDelegateProtocol = protocol;
    }

    @Override
    public void addHeader(String name, String value) {
        mDelegateProtocol.addHeader(name, value);
    }

    @Override
    public void addPathParam(String name, String value) {
        mDelegateProtocol.addPathParam(name, value);
    }

    @Override
    public void addQueryParam(String name, String value) {
        mDelegateProtocol.addQueryParam(name, value);
    }

    @Override
    public AccessToken refreshToken(String clientId, String refreshToken) {
        return mDelegateProtocol.refreshToken(clientId, refreshToken);
    }

    @Override
    public Device registerDevice(Device device) {
        return mDelegateProtocol.registerDevice(device);
    }

    @Override
    public User getMe() {
        return mDelegateProtocol.getMe();
    }

    @Override
    public User getUserInfo(UUID userId) {
        return mDelegateProtocol.getUserInfo(userId);
    }

    @Override
    public Page<Event> getEvents(Map<String, String> options) {
        return mDelegateProtocol.getEvents(options);
    }

    @Override
    public Event getEvent(UUID eventId) {
        return mDelegateProtocol.getEvent(eventId);
    }

    @Override
    public Page<Calendar> getCalendars(Map<String, String> options) {
        return mDelegateProtocol.getCalendars(options);
    }

    @Override
    public Calendar getCalendar(UUID calendarId) {
        return mDelegateProtocol.getCalendar(calendarId);
    }

    @Override
    public Page<Alarm> getAlarms(Map<String, String> options) {
        return mDelegateProtocol.getAlarms(options);
    }

    @Override
    public Alarm getAlarm(UUID alarmId) {
        return mDelegateProtocol.getAlarm(alarmId);
    }
}
