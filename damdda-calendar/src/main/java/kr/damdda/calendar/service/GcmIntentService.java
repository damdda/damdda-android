package kr.damdda.calendar.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.R;
import kr.damdda.calendar.activity.EventDialogActivity;
import kr.damdda.calendar.activity.MainActivity;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.json.AppGsonBuilder;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.util.UUID;

import hugo.weaving.DebugLog;

@DebugLog
public class GcmIntentService extends IntentService {
    public static final String EXTRA_TOTAL_DELETED = "total_deleted";
    public static final String EXTRA_ERROR = "error";

    private static final String TAG = GcmIntentService.class.getSimpleName();
    private AppContext mAppContext = AppContextFactory.getInstance(this);

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        Log.d(TAG, "messageType = " + messageType);
        Log.d(TAG, "extras = " + extras.toString());

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                String errorId = extras.getString(EXTRA_ERROR);
                onError(errorId);
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                int total = extras.getInt(EXTRA_TOTAL_DELETED, 0);
                onDeletedMessages(total);
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                onMessage(intent);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    protected void onRegistered(String registrationId) {

    }

    protected void onUnregistered(String registrationId) {
    }

    protected void onMessage(Intent intent) {
        Bundle extra = intent.getExtras();
        String collapseKey = extra.getString(NotificationHelper.EXTRA_COLLAPSE_KEY);
        String message = extra.getString(NotificationHelper.EXTRA_MESSAGE);
        if(collapseKey == null || message == null) return;

        try {
            JsonObject messageObject = AppGsonBuilder.DEFAULT_FORMAT.fromJson(message, JsonObject.class);
            JsonElement notifyMessageElement = messageObject.get("message");

            //잘못된 형식의 메시지일 경우 패스
            if (notifyMessageElement == null) return;

            String notifyMessage = notifyMessageElement.getAsString();
            UUID target = UUID.fromString(messageObject.get("target").getAsString());
            UUID reference = UUID.fromString(messageObject.get("reference").getAsString());
            sendNotification(notifyMessage, target, reference, collapseKey);

            Event event = null;
            UUID eventId = null;
            switch (collapseKey) {
                case NotificationHelper.EXTRA_COLLAPSE_KEY_OCCUR:
                    try {
                        eventId = target;
                        event = this.getEvent(eventId);
                    }catch(Exception e){
                    }

                    if (event != null) {
                        intent = new Intent(this, EventDialogActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(EventDialogActivity.EXTRA_EVENT, event);

                        startActivity(intent);
                    }
                    break;
                default:
                    break;
            }
        } catch (JsonSyntaxException e) {

        }
    }

    protected void onDeletedMessages(int total) {
    }

    protected void onError(String errorId) {
    }

    private Event getEvent(UUID uuid) {
        return mAppContext.getProtocol().getEvent(uuid);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg, UUID eventId, UUID referenceId, String collapseKey) {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = null;

        switch(collapseKey){
            case NotificationHelper.EXTRA_COLLAPSE_KEY_DAMDDA:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_DETAIL);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_LIKE_USER:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_PROFILE);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_LIKE_COMMENT:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_COMMENT);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_LIKE_EVENT:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_DETAIL);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_EVENT_TAG:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_ADD);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_EVENT_COMMENT:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_COMMENT);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_COMMENT_TAG:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_COMMENT);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_FRIEND_REQUEST:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_PROFILE);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_FRIEND_ACCEPTED:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_PROFILE);
                break;
            case NotificationHelper.EXTRA_COLLAPSE_KEY_OCCUR:
                notificationIntent = new Intent(this, MainActivity.class);
                notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, referenceId.toString());
                notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_DETAIL);
                break;
        }
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setContentText(msg);

        if(notificationIntent != null) {
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentIntent(contentIntent);
        }

        notificationManager.notify(R.id.DEFAULT_NOTIFICATION_ID, builder.build());
    }


    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
        int icon = R.mipmap.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, MainActivity.class);

        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }
}
