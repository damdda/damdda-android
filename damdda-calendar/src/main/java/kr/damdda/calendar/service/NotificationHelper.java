package kr.damdda.calendar.service;

public class NotificationHelper {
    public static final String EXTRA_COLLAPSE_KEY = "collapse_key";
    public static final String EXTRA_MESSAGE = "message";

    public static final String EXTRA_COLLAPSE_KEY_DAMDDA = "DAMDDA";
    public static final String EXTRA_COLLAPSE_KEY_LIKE_USER = "LIKE_USER";
    public static final String EXTRA_COLLAPSE_KEY_LIKE_COMMENT = "LIKE_COMMENT";
    public static final String EXTRA_COLLAPSE_KEY_LIKE_EVENT = "LIKE_EVENT";
    public static final String EXTRA_COLLAPSE_KEY_EVENT_TAG = "EVENT_TAG";
    public static final String EXTRA_COLLAPSE_KEY_EVENT_COMMENT = "EVENT_COMMENT";
    public static final String EXTRA_COLLAPSE_KEY_COMMENT_TAG = "COMMENT_TAG";
    public static final String EXTRA_COLLAPSE_KEY_FRIEND_REQUEST = "FRIEND_REQUEST";
    public static final String EXTRA_COLLAPSE_KEY_FRIEND_ACCEPTED = "FRIEND_ACCEPTED";
    public static final String EXTRA_COLLAPSE_KEY_OCCUR = "OCCUR";
}