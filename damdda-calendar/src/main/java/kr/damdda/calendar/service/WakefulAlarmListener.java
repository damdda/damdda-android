package kr.damdda.calendar.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import java.util.Calendar;

import hugo.weaving.DebugLog;

@DebugLog
public class WakefulAlarmListener implements WakefulIntentService.AlarmListener {
    public static final String TAG = WakefulAlarmListener.class.getSimpleName();

    public void scheduleAlarms(AlarmManager mgr, PendingIntent pi, Context ctxt) {
        System.out.println(pi);
        mgr.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), WakefulAlarmService.INTERVAL, pi);
    }

    public void sendWakefulWork(Context context) {
        WakefulIntentService.sendWakefulWork(context, WakefulAlarmService.class);
    }

    public long getMaxAge() {
        return(WakefulAlarmService.INTERVAL * 2);
    }
}