package kr.damdda.calendar.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import kr.damdda.calendar.R;
import kr.damdda.calendar.activity.MainActivity;

import java.util.UUID;

import hugo.weaving.DebugLog;


@DebugLog
public class AlarmNotificationReceiver extends WakefulBroadcastReceiver {
    public static final String EXTRA_EVENT_ID = "ALARM_EXTRA_EVENT_ID";
    public static final String EXTRA_MESSAGE = "ALARM_EXTRA_MESSAGE";

    public AlarmNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String eventIdString = intent.getStringExtra(AlarmNotificationReceiver.EXTRA_EVENT_ID);
        String msg = intent.getStringExtra(AlarmNotificationReceiver.EXTRA_MESSAGE);
        if (eventIdString == null || msg == null) return;

        UUID eventId = UUID.fromString(eventIdString);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, eventId.toString());
        notificationIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_EDIT);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setContentText(msg);

        builder.setContentIntent(contentIntent);

        notificationManager.notify(R.id.ALARM_NOTIFICATION_ID, builder.build());
    }

}
