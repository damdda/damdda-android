package kr.damdda.calendar.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.dao.AlarmSqliteDao;
import kr.damdda.calendar.dao.EventSqliteDao;
import kr.damdda.calendar.domain.AccessToken;
import kr.damdda.calendar.domain.Alarm;
import kr.damdda.calendar.domain.Calendar;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.domain.Page;
import kr.damdda.calendar.domain.User;
import com.commonsware.cwac.wakeful.WakefulIntentService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import hugo.weaving.DebugLog;

@DebugLog
public class WakefulAlarmService extends WakefulIntentService {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
    public static final long INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
    private static long mLastRuntime = 0;

    private AppContext mAppContext;
    private static AppContext sAppContext;
    private EventSqliteDao mEventSqliteDao;
    private AlarmSqliteDao mAlarmSqliteDao;

    public WakefulAlarmService() {
        super("WakefulAlarmService");

        mAppContext = AppContextFactory.getInstance(this);
        mEventSqliteDao = new EventSqliteDao(mAppContext.getDatabaseHelper());
        mAlarmSqliteDao = new AlarmSqliteDao(mAppContext.getDatabaseHelper());
    }

    @Override
    protected void doWakefulWork(Intent intent) {
        if (mLastRuntime == 0) {
            Date startTime = new Date();

            for (Object obj : mAlarmSqliteDao.query("trigger_time >= ?", DATE_FORMAT.format(startTime)).asList()) {
                registerEventAlarm(this, (Alarm) obj);
            }
        } else {
            Date startTime = new Date(mLastRuntime);
            Date endTime = new Date(new Date().getTime() + INTERVAL);
            for (Alarm alarm : mAlarmSqliteDao.query("trigger_time >= ? and trigger_time < ? ", DATE_FORMAT.format(startTime), DATE_FORMAT.format(endTime)).asList()) {
                registerEventAlarm(this, alarm);
            }
        }
        mLastRuntime = new Date().getTime();
    }

    public static void registerEventAlarm(Context context, Alarm alarm) {
        releaseAlarm(context, alarm);

        if(sAppContext == null){
            sAppContext = AppContextFactory.getInstance(context);
        }
        EventSqliteDao eventSqliteDao = new EventSqliteDao(sAppContext.getDatabaseHelper());

        AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Event event = eventSqliteDao.findById(alarm.getTarget());
        if(event == null) return;

        Date nowTime = new Date();
        Date startTime = alarm.getTriggerTime();

        if(startTime.before(nowTime)) return;

        Intent intent = eventIntent(context, event);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, alarm.getId().hashCode(), intent, 0);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, startTime.getTime(), pIntent);
    }

    // 알람 해제
    public static void releaseAlarm(Context context, Alarm alarm) {
        AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(sAppContext == null){
            sAppContext = AppContextFactory.getInstance(context);
        }
        EventSqliteDao eventSqliteDao = new EventSqliteDao(sAppContext.getDatabaseHelper());

        Event event = eventSqliteDao.findById(alarm.getTarget());
        if(event == null) return;

        Intent intent = eventIntent(context, event);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, alarm.getId().hashCode(), intent, 0);
        mAlarmManager.cancel(pIntent);
        pIntent.cancel();
    }

    public static synchronized void syncEvents(Context context) {
        try {
            Thread.sleep(1000);
        }catch(Exception ignore){
        }

        try {
            AppContext appContext = AppContextFactory.getInstance(context);
            AccessToken accessToken = appContext.getAccessToken();
            User user = appContext.getUser();
            if (accessToken == null || user == null) return;

            Map<String, String> options = new HashMap<String, String>();
            if (appContext.getSyncTime() != null) {
                String syncTime = DATE_FORMAT.format(appContext.getSyncTime());
                options.put("updated_time", ">" + syncTime);
            }
            UUID userId = user.getId();
            if (userId != null) {
                options.put("users.id", userId.toString());
            }
            options.put("sort", "updated_time,created_time,id");
            options.put("size", "5000");

            Page<Event> eventList = appContext.getProtocol().getEvents(options);
            EventSqliteDao eventSqliteDao = new EventSqliteDao(appContext.getDatabaseHelper());
            for (Event event : eventList.getData()) {
                try {
                    if (eventSqliteDao.get(event) == null) {
                        eventSqliteDao.insert(event);
                    } else {
                        eventSqliteDao.update(event);
                    }
                    appContext.setSyncTime(event.getUpdatedTime());
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }

            Page<Alarm> alarmList = appContext.getProtocol().getAlarms(options);
            AlarmSqliteDao alarmSqliteDao = new AlarmSqliteDao(appContext.getDatabaseHelper());
            for (Alarm alarm : alarmList.getData()) {
                try {
                    if (alarmSqliteDao.get(alarm) == null) {
                        alarmSqliteDao.insert(alarm);
                    } else {
                        alarmSqliteDao.update(alarm);
                    }

                    registerEventAlarm(context, alarm);
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static Intent eventIntent(Context context, Event event) {
        Intent intent = new Intent(context, AlarmNotificationReceiver.class);
        intent.putExtra(AlarmNotificationReceiver.EXTRA_EVENT_ID, event.getId().toString());
        intent.putExtra(AlarmNotificationReceiver.EXTRA_MESSAGE, String.format("\"%s\" 일정 알람", event.getName()));

        return intent;
    }
}
