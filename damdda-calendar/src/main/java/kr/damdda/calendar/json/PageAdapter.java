package kr.damdda.calendar.json;

import kr.damdda.calendar.domain.Page;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import hugo.weaving.DebugLog;

@DebugLog
public class PageAdapter implements JsonDeserializer<Page> {
    public static final String FIELD_LINKS = "links";

    @Override
    public Page deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Page page = null;
        if (jsonElement.isJsonObject()) {
            JsonObject object = (JsonObject) jsonElement;
            JsonElement linksElement = object.get(FIELD_LINKS);

            if (linksElement != null && linksElement.isJsonObject()) {
                object.remove(FIELD_LINKS);
                page = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);
//
//                JsonObject linksObject = linksElement.getAsJsonObject();
//                for (Map.Entry<String, JsonElement> entry : linksObject.entrySet()) {
//                    try {
//                        String name = entry.getKey();
//                        Link link = AppGsonBuilder.DEFAULT_FORMAT.fromJson(entry.getValue(), Link.class);
//                        page.addLink(name, link);
//                    }catch (JsonSyntaxException e){
//                    }
//                }
            } else {
                page = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);
            }
        }

        return page;
    }
}
