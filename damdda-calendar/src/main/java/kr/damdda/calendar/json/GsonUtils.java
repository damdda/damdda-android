package kr.damdda.calendar.json;

import com.google.gson.JsonElement;

import kr.lethe.android.util.ReflectUtils;

public class GsonUtils {
    public static <T extends JsonElement> T deepCopy(T source) {
        return (T) ReflectUtils.invokeMethod(source.getClass(), source, "deepCopy");
    }
}
