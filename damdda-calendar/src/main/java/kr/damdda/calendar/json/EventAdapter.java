package kr.damdda.calendar.json;

import kr.damdda.calendar.domain.Event;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.UUID;

import hugo.weaving.DebugLog;

@DebugLog
public class EventAdapter implements JsonDeserializer<Event> {
    public static final String FIELD_LINKS = "links";
    public static final String FIELD_LINK_POSTER = "poster";
    public static final String FIELD_CALENDAR = "calendar";
    public static final String FIELD_OWNER = "owner";
    public static final String FIELD_PARENT = "parent";

    @Override
    public Event deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Event event = null;
        if (jsonElement.isJsonObject()) {
            JsonObject eventObject = (JsonObject) jsonElement;

            JsonElement calendarElement = eventObject.get(FIELD_CALENDAR);
            JsonElement parentElement = eventObject.get(FIELD_PARENT);
            if (calendarElement != null && calendarElement.isJsonObject()) {
                eventObject.remove(FIELD_PARENT);
                eventObject.remove(FIELD_CALENDAR);

                event = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);

                JsonObject calendarObject = calendarElement.getAsJsonObject();
                event.setCalendar(UUID.fromString(calendarObject.get("id").getAsString()));
                event.setCalendarColor(calendarObject.get("color").getAsString());

                if (parentElement != null && parentElement.isJsonObject()) {
                    JsonObject parentObject = parentElement.getAsJsonObject();
                    event.setParent(UUID.fromString(parentObject.get("id").getAsString()));
                }

                JsonElement ownerElement = calendarObject.get(FIELD_OWNER);
                if (ownerElement != null && ownerElement.isJsonObject()) {
                    JsonObject ownerObject = ownerElement.getAsJsonObject();
                    event.setOwner(UUID.fromString(ownerObject.get("id").getAsString()));
                }

                JsonElement linksElement = eventObject.get(FIELD_LINKS);
                if (linksElement != null && linksElement.isJsonObject()) {
                    JsonObject linksObject = linksElement.getAsJsonObject();

                    JsonElement posterElement = linksObject.get(FIELD_LINK_POSTER);
                    if (posterElement != null && posterElement.isJsonObject()) {
                        JsonObject posterObject = posterElement.getAsJsonObject();
                        event.setPosterUrl(posterObject.get("href").getAsString());
                    }
                }
            } else {
                event = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);
            }
        }

        return event;
    }
}
