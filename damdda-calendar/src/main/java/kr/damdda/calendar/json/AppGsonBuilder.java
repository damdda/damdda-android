package kr.damdda.calendar.json;

import kr.damdda.calendar.domain.AccessToken;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;

import java.lang.reflect.Type;

import kr.damdda.calendar.domain.Device;
import kr.damdda.calendar.domain.Event;


public class AppGsonBuilder {
    public static final Gson DEFAULT = new AppGsonBuilder().withDefaultSetting().withDefaultAdapter().build();
    public static final Gson DEFAULT_FORMAT = new AppGsonBuilder().withDefaultSetting().build();

    private boolean mIsSkipIgnoreAnnotation = false;
    private GsonBuilder mGsonBuilder = new GsonBuilder();

    public AppGsonBuilder() {

    }

    public AppGsonBuilder withDefaultSetting() {
        return this
                .withFieldNamingStrategy(FieldNamingPolicy.PREFIX_LOWER_CASE_WITH_UNDERSCORES)
                .withSkipIgnoreAnnotation(true)
                .withDateFormat("yyyyMMdd'T'HHmmss");
    }

    public AppGsonBuilder withDefaultAdapter() {
        return this
                .withRegisterTypeAdapter(AccessToken.class, new AccessTokenAdapter())
                .withRegisterTypeAdapter(Device.class, new DeviceAdapter())
                .withRegisterTypeAdapter(Event.class, new EventAdapter());
//                .withRegisterTypeAdapter(Page.class, new PageAdapter());
    }

    public AppGsonBuilder withFieldNamingStrategy(FieldNamingStrategy fieldNamingStrategy) {
        mGsonBuilder.setFieldNamingStrategy(fieldNamingStrategy);
        return this;
    }

    public AppGsonBuilder withSkipIgnoreAnnotation(boolean isSkipIgnoreAnnotation) {
        mIsSkipIgnoreAnnotation = isSkipIgnoreAnnotation;
        return this;
    }

    public AppGsonBuilder withRegisterTypeHierarchyAdapter(Class<?> baseType, Object typeAdapter) {
        mGsonBuilder.registerTypeHierarchyAdapter(baseType, typeAdapter);
        return this;
    }

    public AppGsonBuilder withRegisterTypeAdapter(Type type, Object typeAdapter) {
        mGsonBuilder.registerTypeAdapter(type, typeAdapter);
        return this;
    }

    public AppGsonBuilder withRegisterTypeAdapterFactory(TypeAdapterFactory factory) {
        mGsonBuilder.registerTypeAdapterFactory(factory);
        return this;
    }

    public AppGsonBuilder withDateFormat(String pattern) {
        mGsonBuilder.setDateFormat(pattern);
        return this;
    }

    public AppGsonBuilder withDateFormat(int dateStyle, int timeStyle) {
        mGsonBuilder.setDateFormat(dateStyle, timeStyle);
        return this;
    }

    public AppGsonBuilder withDateFormat(int style) {
        mGsonBuilder.setDateFormat(style);
        return this;
    }

    public AppGsonBuilder withSerializeNulls() {
        mGsonBuilder.serializeNulls();
        return this;
    }

    public Gson build() {
        if (mIsSkipIgnoreAnnotation) {
            mGsonBuilder.setExclusionStrategies(new IgnoreExclusionStrategy());
        }
        return mGsonBuilder.create();
    }
}
