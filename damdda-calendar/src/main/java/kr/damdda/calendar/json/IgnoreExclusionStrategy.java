package kr.damdda.calendar.json;

import com.google.gson.FieldAttributes;

import kr.damdda.calendar.json.annotation.Ignore;

public class IgnoreExclusionStrategy implements com.google.gson.ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        if (fieldAttributes.getAnnotation(Ignore.class) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean shouldSkipClass(Class<?> clz) {
        if (clz.getAnnotation(Ignore.class) != null) {
            return true;
        }
        return false;
    }
}
