package kr.damdda.calendar.json;

import kr.damdda.calendar.domain.AccessToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

import hugo.weaving.DebugLog;

@DebugLog
public class AccessTokenAdapter implements JsonDeserializer<AccessToken> {
    public static final String FIELD_EXPIRES_TIME = "expires_time";

    @Override
    public AccessToken deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        AccessToken accessToken = null;

        if (jsonElement.isJsonObject()) {
            JsonObject object = (JsonObject) jsonElement;
            JsonElement expiresTimeElement = object.get(FIELD_EXPIRES_TIME);

            if (expiresTimeElement != null && expiresTimeElement.isJsonPrimitive()) {
                object.remove(FIELD_EXPIRES_TIME);
                accessToken = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);

                long expiresTime = expiresTimeElement.getAsLong();
                accessToken.setExpiresTime(new Date(expiresTime));
            } else {
                accessToken = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);
            }
        }

        return accessToken;
    }
}
