package kr.damdda.calendar.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.UUID;

import hugo.weaving.DebugLog;
import kr.damdda.calendar.domain.Device;
import kr.lethe.android.util.ReflectUtils;

@DebugLog
public class DeviceAdapter implements JsonDeserializer<Device> {
    public static final String FIELD_OWNER = "owner";

    @Override
    public Device deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Device device = null;
        if (jsonElement.isJsonObject()) {
            JsonObject object = (JsonObject) jsonElement;
            JsonElement ownerElement = object.get(FIELD_OWNER);

            if (ownerElement != null && ownerElement.isJsonObject()) {
                object.remove(FIELD_OWNER);
                device = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);

                try {
                    Field calendarField = ReflectUtils.getField(device, "mOwner");
                    JsonObject calendarObject = ownerElement.getAsJsonObject();
                    calendarField.set(device, UUID.fromString(calendarObject.get("id").getAsString()));
                } catch (IllegalAccessException ignore) {
                }
            } else {
                device = AppGsonBuilder.DEFAULT_FORMAT.fromJson(jsonElement, type);
            }
        }

        return device;
    }
}
