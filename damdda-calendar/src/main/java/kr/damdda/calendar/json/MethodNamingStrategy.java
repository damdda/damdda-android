package kr.damdda.calendar.json;

import java.lang.reflect.Method;

public interface MethodNamingStrategy {
    public String translateName(Method method);
}
