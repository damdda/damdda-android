package kr.damdda.calendar.json;

import com.google.gson.FieldNamingStrategy;

import java.lang.reflect.Field;

public enum FieldNamingPolicy implements FieldNamingStrategy {
    PREFIX_LOWER_CASE_WITH_UNDERSCORES() {
        @Override
        public String translateName(Field f) {
            return com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES.translateName(f).replaceFirst("m_", "");
        }
    };
}
