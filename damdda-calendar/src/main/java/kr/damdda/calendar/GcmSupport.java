package kr.damdda.calendar;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import kr.damdda.calendar.domain.Device;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import hugo.weaving.DebugLog;
import kr.lethe.android.async.BaseAsyncTask;
import kr.lethe.android.async.OnAsyncTaskListener;

@DebugLog
public class GcmSupport {
    private static final String TAG = GcmSupport.class.getSimpleName();
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Activity mActivity;
    private Context mContext;
    private AppContext mAppContext;
    private GoogleCloudMessaging mGoogleCloudMessaging;

    public GcmSupport(Activity activity) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mAppContext = AppContextFactory.getInstance(mContext);
    }

    public GoogleCloudMessaging getGoogleCloudMessaging() {
        if (mGoogleCloudMessaging == null) {
            mGoogleCloudMessaging = GoogleCloudMessaging.getInstance(mContext);
        }

        return mGoogleCloudMessaging;
    }

    public void registerInBackground(String senderId, final OnAsyncTaskListener<String, Void, String> callback) {
        BaseAsyncTask<String, Void, String> task = new BaseAsyncTask<String, Void, String>() {
            @Override
            protected String doInBackgroundTask(String... params) {
                if (params.length > 0) {
                    try {
                        String senderId = params[0];
                        String registrationId = register(senderId);
                        sendRegistrationIdToBackend(registrationId);

                        return registrationId;
                    } catch (IOException e) {
                        onError(e);
                    }

                    return null;
                } else {
                    throw new IllegalArgumentException("Required senderId");
                }
            }
        };
        task.setOnAsyncTaskListener(callback);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, senderId);
        } else {
            task.execute((String)senderId);
        }
    }

    public String register(String senderId) throws IOException {
        return getGoogleCloudMessaging().register(senderId);
    }

    protected void sendRegistrationIdToBackend(String registrationId) {
        try {
            Device device = mAppContext.getProtocol().registerDevice(new Device(registrationId));
            mAppContext.setDevice(device);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, mActivity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                mActivity.finish();
            }
            return false;
        }
        return true;
    }
}
