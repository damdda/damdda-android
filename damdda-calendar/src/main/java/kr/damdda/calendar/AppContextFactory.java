package kr.damdda.calendar;

import android.content.Context;

public class AppContextFactory {
    private static AppContext sInstance;

    public static AppContext getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppContextImpl(context);
        }
        return sInstance;
    }
}
