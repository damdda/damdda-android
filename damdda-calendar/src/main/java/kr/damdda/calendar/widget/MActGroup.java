package kr.damdda.calendar.widget;

import android.graphics.Color;

import java.util.ArrayList;

import hugo.weaving.DebugLog;

@DebugLog
public class MActGroup
{
    private int dayOfFirstDate;
    private MActWeek[] actWeeks = new MActWeek[6];

    public MActGroup(int dayOfFirstDate)
    {
        this.dayOfFirstDate = dayOfFirstDate;

        for(int i = 0; i < 6; i++)
            actWeeks[i] = new MActWeek(i * 7 - dayOfFirstDate + 1);

    }

    public ArrayList<MWritableAct> getAct(int week, int line)
    {
        return actWeeks[week].getAct(line);
    }


    public void addAct(MAct act) {
        int startWeek = getWeekNum(act.getStartDay());
        int endWeek = getWeekNum(act.getEndDay());

        if(startWeek < 0)
        {
            act.setStartDay(1 - dayOfFirstDate);
            startWeek = 0;
        }

        if(endWeek > 5)
        {
            act.setEndDay(1 - dayOfFirstDate + 42);
            endWeek = 5;
        }

        //일정이 다음주로 넘어간 경우 나누어 주는 작업을 한다.
        MAct[] acts = new MAct[6];
        for(int week = startWeek; week < endWeek; week++)
        {
            acts[week] = new MAct(act.getStartDay(), (week + 1) * 7 - dayOfFirstDate, act.getAct(), act.getBackColorId());
            act.setStartDay((week + 1) * 7 - dayOfFirstDate + 1);
        }
        acts[endWeek] = act;
        //주마다 나눈 각 Act가 들어갈 Line을 조사한다.
        int lineIndex = -1;
        for(int line = 0; line < 3; line ++) {
            boolean flag = true;
            for (int week = startWeek; week <= endWeek; week++) {
                int startDay = acts[week].getStartDay();
                int endDay = acts[week].getEndDay();
                if(!actWeeks[week].isVacantLine(line, startDay, endDay)) {
                    flag = false;
                    break;
                }
            }
            if(flag)
            {
                lineIndex = line;
                break;
            }
        }
        //비어있는 라인을 찾으면 나누어 둔 일정을 모두 저장한다.
        if(lineIndex != -1) {
            for (int week = startWeek; week <= endWeek; week++) {
                actWeeks[week].addAct(acts[week], lineIndex);
            }
        }


    }

    public int getWeekNum(int day)
    {
        return getPos(day) / 7;
    }

    public int getDayOfWeek(int day)
    {
        return getPos(day) - getWeekNum(day) * 7;
    }

    public int getPos(int day)
    {
        return day + dayOfFirstDate - 1;
    }

    private class MActWeek
    {
        private int weekStartDay;
        private MActLine[] actLines = new MActLine[3];

        public MActWeek(int weekStartDay)
        {
            this.weekStartDay = weekStartDay;
            for(int i = 0; i < 3; i++)
            {
                actLines[i] = new MActLine();
            }
        }

        public ArrayList<MWritableAct> getAct(int line)
        {
            return actLines[line].getAct();
        }

        public boolean isVacantLine(int line, int startDay, int endDay)
        {
            int s = startDay - weekStartDay;
            int e = endDay - weekStartDay;
            if(actLines[line].isVacant(s, e))
                return true;
            return false;
        }

        public void addAct(MAct act, int line)
        {
            int s = act.getStartDay() - weekStartDay;
            int e = act.getEndDay() - weekStartDay;

            actLines[line].addAct(s, e, new MActParticle(act.getAct(), act.getBackColorId()));
        }


        private class MActLine
        {
            private ArrayList<MActParticle> acts = new ArrayList<>();
            private int[] actIndexList = new int[7];

            public MActLine()
            {
                //acts의 제일 처음은 빈칸으로 만들어두고 actIndexList에 모두 그 빈칸의 인덱스를 저장한다
                acts.add(new MActParticle("", Color.argb(0, 0, 0, 0)));
                for(int i = 0; i < 7; i++)
                {
                    actIndexList[i] = 0;
                }
            }

            public ArrayList<MWritableAct> getAct()
            {
                ArrayList<MWritableAct> wActs = new ArrayList<>();
                int preIndex = actIndexList[0];
                int period = 0;
                for(int i = 1; i < 7; i++)
                {
                    period++;
                    if(preIndex != actIndexList[i])
                    {
                        wActs.add(new MWritableAct(period, acts.get(preIndex).act, acts.get(preIndex).backColorId));
                        preIndex = actIndexList[i];
                        period = 0;
                    }
                }
                period++;
                wActs.add(new MWritableAct(period, acts.get(preIndex).act, acts.get(preIndex).backColorId));
                return wActs;
            }

            public boolean isVacant(int s, int e)
            {
                for(int i = s; i <= e; i++)
                {
                    if(actIndexList[i] != 0)
                        return false;
                }
                return true;
            }

            public void addAct(int s, int e, MActParticle actParticle)
            {
                int index = acts.size();
                acts.add(actParticle);
                for(int i = s; i <= e; i++)
                    actIndexList[i] = index;
            }
        }
        private class MActParticle
        {
            private String act;
            private int backColorId;

            private MActParticle(String act, int backColorId) {
                this.act = act;
                this.backColorId = backColorId;
            }
        }
    }

}