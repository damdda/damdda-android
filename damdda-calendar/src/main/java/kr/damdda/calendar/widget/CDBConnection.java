package kr.damdda.calendar.widget;

import android.content.Context;

import java.util.List;

import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.dao.EventSqliteDao;
import kr.damdda.calendar.domain.Event;

import hugo.weaving.DebugLog;

@DebugLog
public class CDBConnection {
    private AppContext mAppContext;
    private EventSqliteDao mSqliteDao;

    public CDBConnection()
    {

    }

    public void setContext(Context context)
    {
        mAppContext = AppContextFactory.getInstance(context);
        mSqliteDao = new EventSqliteDao(mAppContext.getDatabaseHelper());
    }

    public void insertTestEvent()
    {
        Event event = new Event();

        mSqliteDao.insert(event);
    }

    public List<Event> getMonthlyEvents(int year, int month)
    {
        return mSqliteDao.getEventsByYearAndMonth(year, month);
    }



    //모든 이벤트 반환
    public List<Object> getEventAll(){
        return mSqliteDao.getAll();
    }

    //새로운 이벤트 등록
    private void insertEvent(Event event){
        mSqliteDao.insert(event);

//        아래와 같이도 이용 가능
//        event.setSqliteDao(mSqliteDao);
//        event.save(); // 이벤트객체가 DB에 없으면 생성
    }

    //새로운 이벤트 수정
    private void updateEvent(Event event){
        mSqliteDao.update(event);

//        아래와 같이도 이용 가능
//        event.setName("이름변경");
//        event.save(); //이벤트 객체가 DB에 있으면 수정
    }


}
