package kr.damdda.calendar.widget;

import hugo.weaving.DebugLog;

//이 객체에는 할 일들이 기간제로 들어가 있어서 바로 쓰기가 가능하다
@DebugLog
public class MWritableAct
{
    public int period;
    public String act;
    public int backColorId;

    public MWritableAct(int period, String act, int backColorId) {
        this.period = period;
        this.act = act;
        this.backColorId = backColorId;
    }

}
