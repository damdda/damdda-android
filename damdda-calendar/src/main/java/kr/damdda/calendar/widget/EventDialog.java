package kr.damdda.calendar.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.damdda.calendar.AppContext;
import kr.damdda.calendar.AppContextFactory;
import kr.damdda.calendar.R;
import kr.damdda.calendar.activity.MainActivity;
import kr.damdda.calendar.domain.Event;
import kr.damdda.calendar.transform.RoundedTransformation;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import kr.lethe.android.util.ScreenUtils;

public class EventDialog extends android.app.Dialog {
    private final AppContext mAppContext;
    @InjectView(R.id.tv_title)
    TextView mEventName;
    @InjectView(R.id.tv_message)
    TextView mEventPeriod;
    @InjectView(R.id.iv_icon)
    ImageView mPoster;

    private Event mEvent;
    private OkHttpClient mHttpClient;
    public EventDialog(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        mAppContext = AppContextFactory.getInstance(context);
        mHttpClient = new OkHttpClient();
        mHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
    }

    public EventDialog(Context context, Event event) {
        this(context);
        setEvent(event);
    }

    public void setEvent(Event event) {
        mEvent = event;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_dialog);
        ButterKnife.inject(this);

        initValue();

        //배경 투명하게 하기
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    private void initValue() {
        if (mEvent == null) return;

        try {
            mEventName.setText(mEvent.getName());
            mEventPeriod.setText(mEvent.getPeriod());
            String posterUrl = mEvent.getPosterUrl();

            mPoster.setImageURI(null);
            if (posterUrl != null) {
                int width = ScreenUtils.convertDpToPixelInt(getContext(), 80);
                int height = width;

                Picasso picasso = new Picasso.Builder(getContext()).downloader(new OkHttpDownloader(mHttpClient)).build();
                picasso.with(getContext()).load(String.format("http://res.cloudinary.com/bad79s/image/fetch/c_thumb,w_%d,h_%d/%s%s", width, height, mAppContext.getApiHost(), posterUrl)).transform(new RoundedTransformation(width, 0)).into(mPoster);
            }
        }catch(Exception e){
            e.printStackTrace();
            try{
                dismiss();
            }catch(Exception ignore){
            }
        }
    }

    @OnClick({R.id.ll_container, R.id.iv_close, R.id.tv_detail, R.id.tv_add})
    public void onClick(View v) {
        switch (v.getId()) {
            // 상세보기
            case R.id.tv_detail:
                Intent detailIntent = new Intent(getContext(), MainActivity.class);
                detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                detailIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, mEvent.getId().toString());
                detailIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_DETAIL);
                getContext().startActivity(detailIntent);
                break;

            // 바로추가
            case R.id.tv_add:
                Intent addEventIntent = new Intent(getContext(), MainActivity.class);
                addEventIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                addEventIntent.putExtra(MainActivity.EXTRA_REFERENCE_ID, mEvent.getId().toString());
                addEventIntent.putExtra(MainActivity.EXTRA_EXEC_TYPE, MainActivity.EXEC_TYPE_EVENT_ADD);
                getContext().startActivity(addEventIntent);
                break;
        }

        dismiss();
    }
}