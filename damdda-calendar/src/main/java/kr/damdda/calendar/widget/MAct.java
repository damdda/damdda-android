package kr.damdda.calendar.widget;

import android.graphics.Color;

import hugo.weaving.DebugLog;

@DebugLog
public class MAct
{
    private int startDay;
    private int endDay;
    private String act;
    private int backColorId;

    public MAct(int startDay, int endDay, String act)
    {
        this.startDay = startDay;
        this.endDay = endDay;
        this.act = act;
        this.backColorId = Color.argb(255, 183, 230, 244);


    }

    public MAct(int startDay, int endDay, String act, int backColorId)
    {
        this.startDay = startDay;
        this.endDay = endDay;
        this.act = act;
        this.backColorId = backColorId;
    }

    public int getStartDay(){return startDay;}
    public void setStartDay(int startDay){this.startDay = startDay;}
    public int getEndDay(){return endDay;}
    public void setEndDay(int endDay){this.endDay = endDay;}
    public String getAct(){return act;}
    public int getBackColorId(){return backColorId;}


}