package kr.damdda.calendar.widget;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.TextView;

import kr.damdda.calendar.R;

import kr.lethe.android.util.ScreenUtils;

/**
 * 커스텀 Dialog 클래스
 * 
 * @author 박상철
 */
public class Dialog extends android.app.Dialog {
	/**
	 * Dialog 테마 리소스
	 */
	private final static int THEME = R.style.Custom_Dialog;
	/**
	 * Dialog 레이아웃 리소스
	 */
	private final static int LAYOUT = R.layout.dialog;

	private Runnable mRunnableShow = null;
	private Runnable mRunnableCancel = null;

	/**
	 * 레이아웃 인자
	 */
	private LayoutParams mLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

	/**
	 * Dialog 레아아웃
	 */
	private View mDialogLayout;

	public Dialog(Context context) {
		super(context, THEME);
		init();

	}

	protected void init() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialogLayout = View.inflate(getContext(), LAYOUT, null);
	}

	/**
	 * Content 가 들어가는 ViewGroup 반환
	 * 
	 * @return
	 */
	public ViewGroup getContent() {
		return (ViewGroup) mDialogLayout.findViewById(R.id.content);
	}

	/**
	 * Command 가 들어가는 ViewGroup 반환
	 * 
	 * @return
	 */
	public ViewGroup getCommand() {
		return (ViewGroup) mDialogLayout.findViewById(R.id.command);
	}

	/**
	 * Content 에 텍스트 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param str 출력 문자열
	 */
	public void setContentView(CharSequence str) {
		int padding = ScreenUtils.convertDpToPixelInt(getContext(), 5);
        TextView view = new TextView(getContext());
		view.setId(android.R.id.text1);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		view.setGravity(Gravity.CENTER);
		view.setPadding(padding, padding, padding, padding);
		view.setText(str);
		setContentView(view);
	}

	/**
	 * Content 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param resource
	 */
	@Override
	public void setContentView(int resource) {
		View view = View.inflate(getContext(), resource, null);
		setContentView(view);
	}

	/**
	 * Content 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param view
	 */
	@Override
	public void setContentView(View view) {
		setContentView(view, null);
	}

	/**
	 * Content 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param view
	 * @param params
	 */
	@Override
	public void setContentView(View view, LayoutParams params) {
		ViewGroup viewGroup = getContent();
		viewGroup.removeAllViews();
		if (params == null) {
			viewGroup.addView(view, viewGroup.getLayoutParams());
		} else {
			viewGroup.addView(view, params);
		}
	}

	/**
	 * Content 에 뷰 삽입<br />
	 * 
	 * @param view
	 */
	public void addContentView(View view) {
		ViewGroup viewGroup = getContent();
		viewGroup.addView(view);
	}

	/**
	 * Content 에 뷰 삽입<br />
	 * 
	 * @param view
	 * @param params
	 */
	@Override
	public void addContentView(View view, LayoutParams params) {
		ViewGroup viewGroup = getContent();
		viewGroup.addView(view, params);
	}

	/**
	 * Command 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param resource
	 */
	public void setCommandView(int resource) {
		View view = View.inflate(getContext(), resource, null);
		setCommandView(view);
	}

	/**
	 * Command 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param resource
	 * @param params
	 */
	public void setCommandView(int resource, LayoutParams params) {
		View view = View.inflate(getContext(), resource, null);
		setCommandView(view, params);
	}

	/**
	 * Command 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param view
	 */
	public void setCommandView(View view) {
		setCommandView(view, null);
	}

	/**
	 * Command 에 뷰 설정<br />
	 * <i>모든 자식을 삭제한 후 추가</i>
	 * 
	 * @param view
	 * @param params
	 */
	public void setCommandView(View view, LayoutParams params) {
		ViewGroup viewGroup = getCommand();
		viewGroup.removeAllViews();
		if (params == null) {
			viewGroup.addView(view, viewGroup.getLayoutParams());
		} else {
			viewGroup.addView(view, params);
		}
		viewGroup.setVisibility(View.VISIBLE);
	}

	/**
	 * Command 에 뷰 삽입<br />
	 * 
	 * @param view
	 */
	public void addCommandView(View view) {
		ViewGroup viewGroup = getCommand();
		viewGroup.addView(view, viewGroup.getLayoutParams());
		viewGroup.setVisibility(View.VISIBLE);
	}

	/**
	 * Command 에 뷰 삽입<br />
	 * 
	 * @param view
	 * @param params
	 */
	public void addCommandView(View view, LayoutParams params) {
		ViewGroup viewGroup = getCommand();
		viewGroup.addView(view, params);
		viewGroup.setVisibility(View.VISIBLE);
	}

	/**
	 * 제목 설정(기본값 : Alert)
	 * 
	 * @param titleId 제목
	 */
	@Override
	public void setTitle(int titleId) {
		setTitle(getContext().getResources().getText(titleId));
	}

	/**
	 * 제목 설정(기본값 : Alert)
	 * 
	 * @param title 제목
	 */
	@Override
	public void setTitle(CharSequence title) {
		TextView textView = (TextView) mDialogLayout.findViewById(R.id.title);
		textView.setText(title);
	}

	/**
	 * 레이아웃 속성 설정
	 * 
	 * @param params
	 */
	public void setLayoutParams(LayoutParams params) {
		if (params == null) {
			throw new NullPointerException("params == null");
		}
		mLayoutParams = params;
	}

	/**
	 * 레이아웃 속성 반환
	 * 
	 * @return
	 */
	public LayoutParams getLayoutParams() {
		return mLayoutParams;
	}

	@Override
	public void show() {
		super.setContentView(mDialogLayout, mLayoutParams);
		if (mRunnableShow != null) {
			mRunnableShow.run();
		}
		super.show();
	}

	@Override
	public void cancel() {
		if (mRunnableCancel != null) {
			mRunnableCancel.run();
		}
		super.cancel();
	}

	/**
	 * Show 시 Runnable 설정
	 * 
	 * @param runnable
	 */
	public void setRunnableShow(Runnable runnable) {
		this.mRunnableShow = runnable;
	}

	/**
	 * Cancel 시 Runnable 설정
	 * 
	 * @param runnable
	 */
	public void setRunnableCancel(Runnable runnable) {
		this.mRunnableCancel = runnable;
	}
}
