/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kr.damdda.calendar.widget;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hugo.weaving.DebugLog;
import kr.damdda.calendar.activity.MainActivity;
import kr.damdda.calendar.domain.Event;

import kr.damdda.calendar.R;

@DebugLog
public class WidgetManager extends AppWidgetProvider {
    private static final String ACTION_PREVIOUS_MONTH= String.format("%s.ACTION.PREVIOUS_MONTH", WidgetManager.class.getName());
    private static final String ACTION_NEXT_MONTH= String.format("%s.ACTION.NEXT_MONTH", WidgetManager.class.getName());
    private static final String ACTION_ON_APP = String.format("%s.ACTION.ON_APP", WidgetManager.class.getName());

    private static final String PREF_MONTH = "month";
    private static final String PREF_YEAR = "year";

    private CDBConnection dbConnection = new CDBConnection();



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int appWidgetId : appWidgetIds) {
            drawWidget(context, appWidgetId);

        }

    }

    private void redrawWidgets(Context context) {
        int[] appWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(
                new ComponentName(context, WidgetManager.class));
        for (int appWidgetId : appWidgetIds) {
            drawWidget(context, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();



        // 이전 버튼 액션
        if (ACTION_PREVIOUS_MONTH.equals(action)) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            Calendar cal = Calendar.getInstance();
            int thisMonth = sp.getInt(PREF_MONTH, cal.get(Calendar.MONTH));
           // int thisYear = sp.getInt(PREF_YEAR, cal.get(Calendar.YEAR));
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.MONTH, thisMonth);
            //cal.set(Calendar.YEAR, thisYear);
            cal.add(Calendar.MONTH, -1);
            sp.edit()
                    .putInt(PREF_MONTH, cal.get(Calendar.MONTH))
                   // .putInt(PREF_YEAR, cal.get(Calendar.YEAR))
                    .apply();
            redrawWidgets(context);

        // 다음 버튼 액션
        } else if (ACTION_NEXT_MONTH.equals(action)) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            Calendar cal = Calendar.getInstance();
            int thisMonth = sp.getInt(PREF_MONTH, cal.get(Calendar.MONTH));
            int thisYear = sp.getInt(PREF_YEAR, cal.get(Calendar.YEAR));
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.MONTH, thisMonth);
            cal.set(Calendar.YEAR, thisYear);
            cal.add(Calendar.MONTH, 1);
            sp.edit()
                    .putInt(PREF_MONTH, cal.get(Calendar.MONTH))
                    .putInt(PREF_YEAR, cal.get(Calendar.YEAR))
                    .apply();
            redrawWidgets(context);

        // 텍스트 버튼 액션
        } else if (ACTION_ON_APP.equals(action)) {
            intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    @Override

    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager,
                                          int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        drawWidget(context, appWidgetId);
    }

    public String YMString(int year, int month){
        String btn_date;
        if(month > 9)
            btn_date = toString().valueOf(year)+"\n"+ toString().valueOf(month);
        else
            btn_date = toString().valueOf(year)+"\n"+"0"+ toString().valueOf(month);
        return btn_date;
    }


    private void drawWidget(Context context, int appWidgetId) {
        dbConnection.setContext(context);

        Calendar cal = Calendar.getInstance();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        Resources res = context.getResources();
       // boolean shortMonthName;
        boolean mini ;
        int numWeeks = 5;


        int minHeightDp1 = 206;


        mini = minHeightDp1 <= res.getInteger(R.integer.max_height_mini_view_dp);
        if (mini) {
            numWeeks = minHeightDp1 <= res.getInteger(R.integer.max_height_mini_view_1_row_dp)
                    ? 1 : 2;
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.damdda_calendar_widget);


        int today = cal.get(Calendar.DAY_OF_YEAR);
        int todayYear = cal.get(Calendar.YEAR);
        int thisYear;
        int thisMonth;

        if (!mini) {
            thisMonth = sp.getInt(PREF_MONTH, cal.get(Calendar.MONTH));
            thisYear = sp.getInt(PREF_YEAR, cal.get(Calendar.YEAR));
            cal.set(Calendar.DAY_OF_MONTH, 1);
            cal.set(Calendar.MONTH, thisMonth);
            cal.set(Calendar.YEAR, thisYear);
        } else {
            thisMonth = cal.get(Calendar.MONTH);
            thisYear = cal.get(Calendar.YEAR);
        }

        Calendar tmpCal = Calendar.getInstance();
        tmpCal.set(thisYear, thisMonth, 1);

        int dayOfFirstDate = tmpCal.get(Calendar.DAY_OF_WEEK) - 1;

        int  lastDay = tmpCal.getActualMaximum(Calendar.DAY_OF_MONTH); // 이달의 마지막 날 얻기
        if(lastDay + dayOfFirstDate > 35){
            numWeeks = 6;
        }

        rv.setTextViewText(R.id.txt_widget_date, YMString(thisYear, thisMonth + 1)); //캘린더 위에 년도와 월 출력


        if (!mini) {
            cal.set(Calendar.DAY_OF_MONTH, 1);
            int monthStartDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, 1 - monthStartDayOfWeek);
        } else {
            int todayDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, 1 - todayDayOfWeek);
        }

        rv.removeAllViews(R.id.ll_widget_date);

        RemoteViews headerRowRv = new RemoteViews(context.getPackageName(),
                R.layout.row_header);
        DateFormatSymbols dfs = DateFormatSymbols.getInstance();
        String[] weekdays = dfs.getShortWeekdays();


        // 셀 헤더를 헤더 컨테이너에 넣음
        for (int day = Calendar.SUNDAY; day <= Calendar.SATURDAY; day++) {

            RemoteViews dayRv = new RemoteViews(context.getPackageName(), R.layout.cell_header);
            dayRv.setTextViewText(android.R.id.text1, transString(weekdays[day]));

            if(day == Calendar.SATURDAY){
                dayRv.setTextColor(android.R.id.text1, Color.BLUE);}
            else if(day == Calendar.SUNDAY){
                dayRv.setTextColor(android.R.id.text1, Color.RED);
            }
            else dayRv.setTextColor(android.R.id.text1, Color.BLACK);
            Log.d("sdfsdfsdf",weekdays[day]);
            headerRowRv.addView(R.id.row_container, dayRv);
        }
        rv.addView(R.id.ll_widget_date, headerRowRv);



        boolean inMonth = cal.get(Calendar.MONTH) == thisMonth;
        boolean inYear = cal.get(Calendar.YEAR) == todayYear;

        for (int week = 0; week < numWeeks; week++) {
            RemoteViews rowRv = new RemoteViews(context.getPackageName(), R.layout.row_week);
            for (int day = 0; day < 7; day++) {

                boolean isToday = inYear && inMonth && (cal.get(Calendar.DAY_OF_YEAR) == today);

                // boolean isFirstOfMonth = cal.get(Calendar.DAY_OF_MONTH) == 1;
                int cellLayoutResId = R.layout.cell_day;
                if (isToday) {
                    cellLayoutResId = R.layout.cell_today;
                } else if (inMonth) {
                    cellLayoutResId = R.layout.cell_day_this_month;
                }
                RemoteViews cellRv = new RemoteViews(context.getPackageName(), cellLayoutResId);

                //일요일날짜의 숫자들은 빨간색
                if(cal.get(Calendar.DAY_OF_WEEK ) == 1){
                    cellRv.setTextColor(android.R.id.text1, Color.RED);
                }
                else if(cal.get(Calendar.DAY_OF_WEEK) == 7){
                    cellRv.setTextColor(android.R.id.text1, Color.BLUE);
                }

                String textday = "  "+Integer.toString(cal.get(Calendar.DAY_OF_MONTH));

                //cellRv.setTextViewText(android.R.id.text1, Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
                cellRv.setTextViewText(android.R.id.text1, textday);
                rowRv.addView(R.id.row_container, cellRv);
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
            rv.addView(R.id.ll_widget_date, rowRv);
        }

        int[] cellBoxIds = new int[]{R.layout.cell_box_weight1,
                R.layout.cell_box_weight2,
                R.layout.cell_box_weight3,
                R.layout.cell_box_weight4,
                R.layout.cell_box_weight5,
                R.layout.cell_box_weight6};

        List<Event> dbData = dbConnection.getMonthlyEvents(thisYear, thisMonth + 1);
        System.out.println(dbData);



        //actGroup에 그 달에 표시할 항목들을 넣기만 하면 출력이 된다.
        MActGroup actGroup = new MActGroup(dayOfFirstDate);
        for(int i = 0; i < dbData.size(); i++)
        {
            Event event = dbData.get(i);
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(event.getStartTime());
            Calendar endCal = Calendar.getInstance();
            endCal.setTime(event.getEndTime());
            try {
                int argb = Color.parseColor(event.getComputedColor());
/*
                int r = (argb) & 0xFF;
                int g = (argb >> 8) & 0xFF;
                int b = (argb >> 16) & 0xFF;
                int a = (argb >> 24) & 0xFF;
*/
                actGroup.addAct(new MAct(startCal.get(Calendar.DATE), endCal.get(Calendar.DATE), event.getName(), argb));
            }
            catch(Exception str){
                actGroup.addAct(new MAct(startCal.get(Calendar.DATE), endCal.get(Calendar.DATE), event.getName()));
            }


        }

        //actGroup.addAct(new MAct(-2, 4, "-2일부터 4일까지"));
/*        actGroup.addAct(new MAct(2, 4, "2일부터 4일까지"));
        actGroup.addAct(new MAct(2, 2, "2일만", Color.RED));
        actGroup.addAct(new MAct(3, 3, "3일만"));
        actGroup.addAct(new MAct(3, 6, "3일부터 6일까지"));
        actGroup.addAct(new MAct(11, 12, "11일부터 12일까지"));
        actGroup.addAct(new MAct(11, 13, "11일부터 13일까지", Color.YELLOW));

        actGroup.addAct(new MAct(17, 30, "테스트중이다 히히2", Color.GREEN));
        actGroup.addAct(new MAct(17, 22, "테스트중이다 히히"));
        actGroup.addAct(new MAct(17, 22, "테스트중이다 히히3"));*/

        rv.removeAllViews(R.id.ll_widget_act);

        RemoteViews tmpLineRv = new RemoteViews(context.getPackageName(), R.layout.cell_top_line); //row_header에 맞춰 간격을 만든다.
        rv.addView(R.id.ll_widget_act, tmpLineRv);

        for (int week = 0; week < numWeeks; week++) {
            RemoteViews weekRv = new RemoteViews(context.getPackageName(), R.layout.row_week2);

            tmpLineRv = new RemoteViews(context.getPackageName(), R.layout.cell_line); //날짜가 나오는 부분은 빈칸으로 채운다


            weekRv.addView(R.id.row_container, tmpLineRv);

            for(int line = 0; line < 3; line++) {
                tmpLineRv = new RemoteViews(context.getPackageName(), R.layout.cell_line);

                ArrayList<MWritableAct> wActs = actGroup.getAct(week, line);

                for(int i = 0; i < wActs.size(); i++) {
                    MWritableAct wAct = wActs.get(i);
                    int boxIdIndex = wAct.period - 1;
                    if (boxIdIndex > 5)
                        boxIdIndex = 0;
                    RemoteViews boxRv = new RemoteViews(context.getPackageName(), cellBoxIds[boxIdIndex]);
                    boxRv.setInt(R.id.row_box_container, "setBackgroundColor", wAct.backColorId);
                    RemoteViews textRv = new RemoteViews(context.getPackageName(), R.layout.text_line);
                    textRv.setTextColor(android.R.id.text1, Color.WHITE);
                    String textday = wAct.act;
                    //cellRv.setTextViewText(android.R.id.text1, Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
                    textRv.setTextViewText(android.R.id.text1, textday);
                    boxRv.addView(R.id.row_box_container, textRv);
                    tmpLineRv.addView(R.id.row_line_container, boxRv);
                }
                weekRv.addView(R.id.row_container, tmpLineRv);
            }


            rv.addView(R.id.ll_widget_act, weekRv);
        }




        // 이전버튼 누르면 이전달 출력
        rv.setOnClickPendingIntent(R.id.btn_calendar_prevmonth, PendingIntent.getBroadcast(context, 0,
                        new Intent(context, WidgetManager.class).setAction(ACTION_PREVIOUS_MONTH),PendingIntent.FLAG_UPDATE_CURRENT));

        // 다음버튼 누르면 다음달 출력
        rv.setOnClickPendingIntent(R.id.btn_calendar_nextmonth, PendingIntent.getBroadcast(context, 0,
                new Intent(context, WidgetManager.class).setAction(ACTION_NEXT_MONTH),PendingIntent.FLAG_UPDATE_CURRENT));

        // 텍스트누르면 달력앱 켜짐
        rv.setOnClickPendingIntent(R.id.ll_widget_date, PendingIntent.getBroadcast(context, 0,
                new Intent(context, WidgetManager.class).setAction(ACTION_ON_APP),PendingIntent.FLAG_UPDATE_CURRENT));


                rv.setViewVisibility(R.id.selectmonth, numWeeks <= 1 ? View.GONE : View.VISIBLE);

        appWidgetManager.updateAppWidget(appWidgetId, rv);
    }

    private String transString(String text1){

        if(text1 == "일"){text1 = "S";}

        return text1;
    }


}
