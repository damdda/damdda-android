package kr.damdda.calendar.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;

import java.util.Calendar;

import hugo.weaving.DebugLog;

@DebugLog
public class CalendarLayout extends LinearLayout{

    private Calendar rightNow;
    private int mSelect = -1;

    public CalendarLayout(Context context) {
        super(context);
        init();
    }

    public CalendarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CalendarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    public void init()
    {
        updateDate();
        //makeCalendar(rightNow.get(Calendar.YEAR), rightNow.get(Calendar.MONTH));
    }


    public void updateDate()
    {
        rightNow = Calendar.getInstance();
    }


    public void update()
    {

    }

    @Override
    protected void onDraw(Canvas c) {

        super.onDraw(c);
    }

    private void makeCalendar(int year, int month)
    {

        this.removeAllViews();

        Calendar calp = Calendar.getInstance();
        calp.set(year, month, 1);

        int startDayOfweek = calp.get(Calendar.DAY_OF_WEEK);
        int maxDay = calp.getActualMaximum ((Calendar.DAY_OF_MONTH));
        final Oneday[] oneday = new Oneday[maxDay + startDayOfweek];

        int dayCnt = -startDayOfweek;
        int maxRow = (int)((startDayOfweek + maxDay) / 7);
        int maxColumn = 7;


        int oneday_width = getWidth();
        int oneday_height = getHeight();

        oneday_height = ((((oneday_height >= oneday_width)?oneday_height:oneday_width) - this.getTop()) / (maxRow+1))-10;
        oneday_width = (oneday_width / maxColumn)+1;


        for(int i = 0; i < maxRow; i++)
        {
            LinearLayout tr = new LinearLayout(getContext());
            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            for(int j = 0; j < maxColumn; j++)
            {
                if(dayCnt >= maxDay)
                    break;

                oneday[dayCnt] = new Oneday(getContext());

                oneday[dayCnt].isToday = false;
                oneday[dayCnt].setTextActcntSize(14);
                oneday[dayCnt].setTextActcntColor(Color.BLACK);
                oneday[dayCnt].setTextActcntTopPadding(18);
                oneday[dayCnt].setBgSelectedDayPaint(Color.rgb(0, 162, 232));
                oneday[dayCnt].setBgTodayPaint(Color.LTGRAY);
                oneday[dayCnt].setBgActcntPaint(Color.rgb(251, 247, 176));
                oneday[dayCnt].setLayoutParams(new TableRow.LayoutParams(oneday_width,oneday_height));
                oneday[dayCnt].setId(dayCnt); /* 생생한 객체를 구별할수 있는 id넣기기*/
                if(dayCnt >= 0) {
                    oneday[dayCnt].setDayOfWeek(dayCnt % 7 + 1);
                    oneday[dayCnt].setDay(Integer.valueOf(dayCnt + 1).intValue());

                    oneday[dayCnt].setDay(dayCnt + 1);
                    oneday[dayCnt].setMonth(month);
                    oneday[dayCnt].setYear(year);

                    if(dayCnt + 1 == rightNow.get(Calendar.DATE) &&
                            month == rightNow.get(Calendar.MONTH) &&
                            year == rightNow.get(Calendar.YEAR))
                        oneday[dayCnt].isToday = true;


                    oneday[dayCnt].setOnLongClickListener(new OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            //Toast.makeText(context, iYear+"-"+iMonth+"-"+oneday[v.getId()].getTextDay(), Toast.LENGTH_LONG).show();
                            return false;
                        }
                    });

                    oneday[dayCnt].setOnTouchListener(new OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            if(oneday[v.getId()].getTextDay() != "" && event.getAction() == MotionEvent.ACTION_UP)
                            {
                                if(mSelect != -1){
                                    oneday[mSelect].setSelected(false);
                                    oneday[mSelect].invalidate();
                                }
                                oneday[v.getId()].setSelected(true);
                                oneday[v.getId()].invalidate();
                                mSelect = v.getId();

                                //Log.d("hahaha", oneday[mSelect].getMonth()+"-"+ oneday[mSelect].getDay());


                            }
                            return false;
                        }
                    });

                    oneday[dayCnt].setTextDay(String.valueOf(dayCnt + 1)); /* 요일, 일자 넣기 */

                }

                oneday[dayCnt].invalidate();
                tr.addView(oneday[dayCnt]);
                dayCnt++;

            }
            this.addView(tr,new LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        }
    }





}
